package pendrawing;

import java.awt.Point;

import pendrawing.brush.Brush;
import pendrawing.brush.BrushCreator;
import pendrawing.brush.BrushLine;
import processing.core.*;

public class PenDrawing extends PApplet {

	private static final long serialVersionUID = 5864052341902352601L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"pendrawing.PenDrawing"});
	}
	
	private final int thicknessSteps = 20;
	private final int thicknessMin = 0;  // diameter
	private final int thicknessMax = 6; // diameter
	private final float hardness = 0.2f;
	
	private final float innerOpacity = 1f;
	private final float outerOpacity = 0.0f;
	
	private float followX, followY;
	private float followTightness = 0.1f;
	private float followSpeed = 0;
	private float maxSpeed = 40;
	
	private Brush[] shapes;
	private PImage target;
	
	private int color = 0x1f1303;
	
	public void settings() {
		size(800,600,P3D);
	}
	
	
	public void setup() {
		
		background(240);
		
		target = new PImage(width,height);
		target.loadPixels();
		target.copy(this.g, 0, 0, width, height, 0, 0, width, height);
		
		shapes = new Brush[thicknessSteps];
		for (int i=0;i<shapes.length;i++) {
			float p = ((float)i/(shapes.length-1));
			float outerRadius = thicknessMin + p*thicknessMax;
			float innerRadius = outerRadius * hardness;
			shapes[i] = BrushCreator.createCircular(innerRadius, outerRadius, innerOpacity, outerOpacity, color);
		}
	}
	
	
	public void mousePressed() {
		if (mouseButton==RIGHT) {
			background(240);
			target.copy(this.g, 0, 0, width, height, 0, 0, width, height);
			target.updatePixels();
		}
	}
	
	public void mouseDragged() {
		if (mouseButton==LEFT) {
			BrushLine l = new BrushLine(new Point(pmouseX,pmouseY),new Point(mouseX,mouseY));
			float speed = Math.min(followSpeed, maxSpeed)/maxSpeed;
			int i=(int) ((1-speed)*(shapes.length-1));
			l.drawVersionOld(target,shapes[i]);
		}
	}	
	
	
	public void draw() {
		image(target,0,0);
		
		ellipseMode(CENTER);
		noFill();
		stroke(255,0,0);
		ellipse(followX,followY,5,5);
		
		float dx = followTightness*(mouseX-followX);
		float dy = followTightness*(mouseY-followY);
		followX += dx;
		followY += dy;
		
		followSpeed = sqrt(dx*dx+dy*dy);
		rectMode(CORNERS);
		fill(255,0,0);
		rect(20,height-20-followSpeed,40,height-20);
		
		//println(followSpeed);
		
	}

}
