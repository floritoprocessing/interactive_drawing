package pendrawing.brush;

import java.awt.Point;

import processing.core.PImage;

public abstract class AbstractBrush extends BrushTools {

	public abstract int size();
	public abstract Point getPoint(int i);
	public abstract int getRGB(int i);
	public abstract float getOpacity(int i);
	
	
	public void drawAt(PImage pg, float x, float y) {
		int x0 = (int)x;
		int x1 = x0+1;
		int y0 = (int)y;
		int y1 = y0+1;
		float px1 = x-x0, px0 = 1-px1;
		float py1 = y-y0, py0 = 1-py1;
		float p00 = px0*py0;
		float p01 = px0*py1;
		float p10 = px1*py0;
		float p11 = px1*py1;
		for (int i=0;i<size();i++) {
			int px = getPoint(i).x;
			int py = getPoint(i).y;
			int col = getRGB(i);
			float opacity = getOpacity(i);
			int tx0 = x0+px;
			int tx1 = x1+px;
			int ty0 = y0+py;
			int ty1 = y1+py;
			int bg00 = pg.get(tx0, ty0);
			int bg01 = pg.get(tx0, ty1);
			int bg10 = pg.get(tx1, ty0);
			int bg11 = pg.get(tx1, ty1);
			float p1 = opacity * p00;
			float p0 = 1-p1;
			pg.set(tx0, ty0, mixRGB(bg00, col, p0, p1));
			p1 = opacity * p01;
			p0 = 1-p1;
			pg.set(tx0, ty1, mixRGB(bg01, col, p0, p1));
			p1 = opacity * p10;
			p0 = 1-p1;
			pg.set(tx1, ty0, mixRGB(bg10, col, p0, p1));
			p1 = opacity * p11;
			p0 = 1-p1;
			pg.set(tx1, ty1, mixRGB(bg11, col, p0, p1));
		}
	}
	
	/**
	 * Used for simple line drawings
	 * @param pg
	 * @param x
	 * @param y
	 */
	public void drawAt(PImage pg, int x, float y) {
		float p1 = y-(int)y;
		float p0=1-p1;
		for (int i=0;i<size();i++) {
			int xx = x + getPoint(i).x;
			int y0=(int)y + getPoint(i).y;
			int y1=(int)y + 1 + getPoint(i).y;
			int c0 = pg.get(xx,y0);
			int c1 = pg.get(xx,y1);
			int[] outCol = renderRGB(c0, c1, p0, p1, getOpacity(i), getRGB(i));
			pg.set(xx,y0,0xff<<24|outCol[0]);
			pg.set(xx,y1,0xff<<24|outCol[1]);
		}
	}
	
	/**
	 * Used for simple line drawings
	 * @param pg
	 * @param x
	 * @param y
	 */
	public void drawAt(PImage pg, float x, int y) {
		float p1=x-(int)x;
		float p0=1-p1;
		for (int i=0;i<size();i++) {
			int x0=(int)x + getPoint(i).x;
			int x1=(int)x + 1 + getPoint(i).x;
			int yy = y + getPoint(i).y;
			int c0 = pg.get(x0,yy);
			int c1 = pg.get(x1,yy);
			int[] outCol = renderRGB(c0, c1, p0, p1, getOpacity(i), getRGB(i));
			pg.set(x0,yy,0xff<<24|outCol[0]);
			pg.set(x1,yy,0xff<<24|outCol[1]);
		}
	}
	
	public void drawAt(PImage pg, int x, int y) {
		for (int i=0;i<size();i++) {
			int xx=x + getPoint(i).x;
			int yy=y + getPoint(i).y;
			int bg = pg.get(xx,yy);
			pg.set(xx,yy,0xff<<24|mixRGB(bg, getRGB(i),	(1-getOpacity(i)), getOpacity(i)));
		}
	}
	
	
	
	private static int[] renderRGB(int bg0, int bg1, float p0, float p1, float opacity, int rgb) {
		float dp0 = p0*opacity;
		float dp1 = p1*opacity;
		int col0 = 0, col1 = 0;
		col0 = mixRGB(bg0,rgb,(1-dp0),dp0);
		col1 = mixRGB(bg1,rgb,(1-dp1),dp1);
		return new int[] {col0,col1};
	}
	
	

}
