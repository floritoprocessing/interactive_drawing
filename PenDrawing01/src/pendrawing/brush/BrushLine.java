package pendrawing.brush;

import java.awt.Point;

import processing.core.PImage;


//TODO

/**
 * http://www.cs.unc.edu/~mcmillan/comp136/Lecture6/Lines.html
 * Drawing a line with a brush like this is not correct yet
 * This is not a brush, it's actually a Symbol (using Adobe terms)
 * 
 * Instead,
 * 
 * - Create image that can contain the line
 * - Draw the line just using premultiplied RGB
 * - Set all alpha to 0
 * - Draw the line just using alpha, keeping always maximum value of alpha
 * - Now draw the created image onto the target
 * (it's now possible to also use blending modes)  
 *
 * @author mgraf
 *
 */
public class BrushLine {
	
	private Point p0, p1;
		
	public BrushLine(Point p0, Point p1) {
		this.p0 = new Point(p0);
		this.p1 = new Point(p1);
	}
	
	public BrushLine(int x0, int y0, int x1, int y1) {
		this.p0 = new Point(x0,y0);
		this.p1 = new Point(x1,y1);
	}
	
	public void drawVersionOld(PImage pg, AbstractBrush shape) {
		int x0 = p0.x;
		int x1 = p1.x;
		int y0 = p0.y;
		int y1 = p1.y;
		int dx = x1-x0;
		int dy = y1-y0;
		float d = (float)Math.sqrt(dx*dx+dy*dy);
		float m = dx/d;
		float n = dy/d;
		for (float path=0;path<d;path++) {
			float x = x0 + m*path;
			float y = y0 + n*path;
			shape.drawAt(pg, x, y);
		}
		pg.updatePixels();
	}
	
	public void draw(PImage pg, AbstractBrush shape) {
		int x0 = p0.x;
		int x1 = p1.x;
		int y0 = p0.y;
		int y1 = p1.y;
		int dx = x1-x0;
		int dy = y1-y0;
		boolean horizontal = true;
		if (Math.abs(dx)<Math.abs(dy)) {
			horizontal = false;
		}
		if (horizontal) {
			int x=x0;
			float t = y0;
			int xadd = dx<0?-1:1;
			float m = (float) dy / (float) dx;
			m*=xadd;
			while(x!=x1) {
				shape.drawAt(pg, x, t);
				x+=xadd;
				t+=m;
			}
		}
		else {
			int y=y0;
			float t = x0;
			int yadd = dy<0?-1:1;
			float m = (float)dx / (float)dy;
			m*=yadd;
			while (y!=y1) {
				shape.drawAt(pg, t, y);
				t+=m;
				y+=yadd;
			}
		}
	}	

	public void translate(int xo, int yo) {
		p0.x += xo;
		p1.x += xo;
		p0.y += yo;
		p1.y += yo;
	}
	

}
