package pendrawing.brush;

import java.awt.Point;

public class DotBrush extends AbstractBrush {

	private final Point point;
	private final int rgb;
	private final float opacity;
	
	public DotBrush(int x, int y) {
		point = new Point(x,y);
		rgb = 1;
		opacity = 1;
	}

	public DotBrush(int x, int y, int rgb) {
		point = new Point(x,y);
		this.rgb = rgb;
		opacity = 1;
	}

	public DotBrush(int x, int y, int rgb, float opacity) {
		point = new Point(x,y);
		this.rgb = rgb;
		this.opacity = opacity;
	}

	public int size() {
		return 1;
	}

	public Point getPoint(int i) {
		return point;
	}

	public int getRGB(int i) {
		return rgb;
	}

	public float getOpacity(int i) {
		return opacity;
	}
	
	
}
