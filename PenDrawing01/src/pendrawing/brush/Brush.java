package pendrawing.brush;

import java.awt.Point;

public class Brush extends AbstractBrush {
	
	private Point[] point;
	private int[] color;
	private float[] opacity;
	
	public Brush(Point[] inPoints, int[] inColor, float[] inOpacity) {
		point = new Point[inPoints.length];
		System.arraycopy(inPoints, 0, point, 0, inPoints.length);
		
		if (inColor.length!=inPoints.length) {
			if (inColor.length==1) {
				color = new int[inPoints.length];
				for (int i=0;i<color.length;i++) {
					color[i] = inColor[0];
				}
			} else {
				throw new RuntimeException("rgb array length has to be equal to point array length or 1");
			}
		} else {
			color = new int[inColor.length];
			System.arraycopy(inColor, 0, color, 0, inColor.length);
		}
		
		if (inOpacity.length!=inPoints.length) {
			if (inOpacity.length==1) {
				opacity = new float[inPoints.length];
				for (int i=0;i<opacity.length;i++) {
					opacity[i] = inOpacity[0];
				}
			} else {
				throw new RuntimeException("opacity array length has to be equal to point array length or 1");
			}
		} else {
			opacity = new float[inOpacity.length];
			System.arraycopy(inOpacity, 0, opacity, 0, inOpacity.length);
		}

	}
	
	public int size() {
		return point.length;
	}

	public Point getPoint(int i) {
		return point[i];
	}

	public int getRGB(int i) {
		return color[i];
	}

	public float getOpacity(int i) {
		return opacity[i];
	}
}
