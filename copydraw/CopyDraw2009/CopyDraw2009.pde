int maxCreated = 0;
SpaceLine[] sl=new SpaceLine[500];

int DIVISIONS = 60;
int CONNECTION = 15;

float rotY=0.0;
float smallRot=0.0;
boolean go = false;
int lastCreated = 0;

float followX=0, followY=0;

void setup() {
  size(450,600,P3D);
  stroke(0);
  background(255);
  smooth();
  for (int i=0;i<sl.length;i++) {
    sl[i] = null;
  }
  
  smallRot=TWO_PI/DIVISIONS;//0.1;
}

void mousePressed() {
  if (!go) {
    go=true;
    followX = mouseX;
    followY = mouseY;
  }
}


void shiftAll() {
  SpaceLine[] tmp = new SpaceLine[sl.length];
  arrayCopy(sl,0,tmp,0,sl.length);
  arrayCopy(tmp,0,sl,1,sl.length-1);
}

void draw() {
  
  followX = 0.9*followX + 0.1*mouseX;
  followY = 0.9*followY + 0.1*mouseY;
  
  if (go) {
    background(255);
    translate(width/2,0,00);
  
    shiftAll();
    //sl[0]=new SpaceLine(mouseX-width/2,mouseY,0,-frameCount*smallRot);
    sl[0]=new SpaceLine(followX-width/2,followY,0,-frameCount*smallRot);
    sl[0].col = 500;
  
    rotY=0.0;
    rotateY(-TWO_PI*CONNECTION/DIVISIONS);
    //rotateY(-TWO_PI/CONNECTION);
    rotateY((frameCount*smallRot)%TWO_PI);
  
    for (int i=0;i<sl.length;i++) {
      
      if (sl[i]!=null) {
        if (i==1) {
          sl[i].neighbour = sl[0];
        } else if (i==CONNECTION) {
          sl[i].bottom = sl[0];
        }
        pushMatrix();
        rotateY(sl[i].yrot);
        sl[i].draw();
        popMatrix();
      }
    }
  
  } else {
    frameCount--;
  }

}


