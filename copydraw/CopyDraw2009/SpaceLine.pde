class SpaceLine {
  float x,y,z;
  float yrot;
  float col = 255;
  SpaceLine bottom = null;
  SpaceLine neighbour = null;
  
  SpaceLine(float x, float y, float z, float yrot) {
    this.x=x; 
    this.y=y; 
    this.z=z;
    this.yrot = yrot;
  }
  
  void draw() {
      if (col>0) {
        col--;
      }
      
      if (bottom!=null) {
        stroke(0,0,0,min(col,255));
        
        line(x,y,z,bottom.x,bottom.y,bottom.z);
        if (neighbour!=null) {
          line(x,y,z,neighbour.x,neighbour.y,neighbour.z);
        }
      }
        
      
  }
  
}
