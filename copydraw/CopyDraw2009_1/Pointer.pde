class Pointer {
  
  float x, y;
  float spd;
  float dir;
  
  
  Pointer() {
    init();
  }
  
  void init() {
    x = random(width);
    y = random(height);
    spd = random(0.1,0.2);
    dir = random(TWO_PI);
  }
  
  void move() {
    dir += random(-0.1,0.1);
    spd *= 0.99;
    
    float futX = x + spd*cos(dir);
    float futY = y + spd*cos(dir);
    
    while (futX<0||futX>=width||futY<0||futY>=height) {
      dir = random(TWO_PI);
      futX = x + spd*cos(dir);
      futY = y + spd*cos(dir);
    }

    x = futX;
    y = futY;
    
  }
  
}
