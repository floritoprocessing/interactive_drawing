import processing.opengl.*;

int MAX_AMOUNT = 5000;

PVector[] pos;// = new PVector[MAX_AMOUNT];
boolean[] end;// = new boolean[pos.length];

private boolean go;// = true;
int DIVISIONS = 60;
float STEP = TWO_PI/DIVISIONS;

boolean SIMULATION;// = false;
float drawX, drawY;

boolean isDrawing;// = true;

Pointer pointer;// = new Pointer();

void setup() {
  size(450,600,OPENGL);
  pointer = new Pointer();
  pos = new PVector[MAX_AMOUNT];
  end = new boolean[pos.length];
  isDrawing = true;
  SIMULATION = false;
  go = true;
  background(255);
  smooth();
}


void mousePressed() {
  if (mouseButton==RIGHT) {
    go=true;
    pos = new PVector[MAX_AMOUNT];
    end = new boolean[pos.length];
    frameCount=0;
  }
}

void keyPressed() {
  if (key=='c') setup();
}

void mouseReleased() {
  if (mouseButton==LEFT && !SIMULATION) stopDrawing();
}

void stopDrawing() {
  for (int i=0;i<=DIVISIONS;i++) {
    end[i] = true;
  }
}


void shiftAll() {
  PVector[] tmp = new PVector[pos.length];
  arrayCopy(pos,0,tmp,0,pos.length);
  arrayCopy(tmp,0,pos,1,pos.length-1);
  boolean[] tmp2 = new boolean[end.length];
  arrayCopy(end,0,tmp2,0,end.length);
  arrayCopy(tmp2,0,end,1,end.length-1);
}

void vertex(PVector vec) {
  vertex(vec.x,vec.y,vec.z);
}


void draw() {
  
  int iterations = SIMULATION?10:1;
  
  for (int ii=0;ii<iterations;ii++) {
  
  // interavtive:
  if (!SIMULATION) {
    isDrawing = mousePressed && mouseButton==LEFT;
    drawX = mouseX;
    drawY = mouseY;
  }
  
  //simaulation:
  else {
    if (!isDrawing) {
      if (Math.random()<0.1) {
        isDrawing=true;
        pointer.init();
      }
    }
    else {
      if (Math.random()<0.002) {
      //if (pointer.spd<0.01) {
        isDrawing=false;
        stopDrawing();
      }
    }
    pointer.move();
    
    drawX = pointer.x;//simX;
    drawY = pointer.y;//simY;
  }
  
  
  if (!go) {
    frameCount--;
  } else {
    
    if (isDrawing) {
      shiftAll();
      float rd = ((frameCount-1)*STEP)%TWO_PI;
      float r = -abs(drawX-width/2);
      float x = cos(rd)*r;
      float z = sin(rd)*r;
      pos[0] = new PVector(x,drawY,z);
      end[0] = false;
    }
    
    
    if (ii==0) drawAll();
    
    
    
  }
  
  frameCount++;
  }
  frameCount--;
}

void drawAll() {
  background(16);
    lights();
    shininess(5);
    specular(255,255,255);
    
    translate(width/2,0,0);
    rotateY((frameCount-1)*STEP);
    for (int i=0;i<pos.length;i++) {
      if (pos[i]!=null) {
        
        int other = i-DIVISIONS;
        if (other>0) {
          
          if (!end[i]) {
          int j = i-1;
          int otherJ = i-DIVISIONS-1;
          noStroke();
          //fill(255,64,16);
          fill(0,100,200);
          beginShape(QUADS);
          vertex(pos[i]);
          vertex(pos[j]);
          vertex(pos[otherJ]);
          vertex(pos[other]);
          endShape();
          }
          
        } else {
          if (isDrawing) {
             noFill();
             //stroke(255,64,16,64);
             stroke(255);
             point(pos[i].x,pos[i].y,pos[i].z);
          }
        }
      }
    }
}
