int nrOfLines=5000;
int nrOfShowLines=500;
SpaceLines[] sl=new SpaceLines[nrOfLines+1];

//Bird[] bird=new Bird[nrOfBirds+1];
//bird[i]=new Bird(i);

int counter=0;
boolean go=false;
float rotY=0.0,rotY2=0.0;
float smallRot=0.0;

void setup() {
  size(400,400,P3D);
  stroke(0);//smooth();
  //lights();
  smallRot=0.1;
}

void draw() {
  background(255);
  if (go) {
    translate(width/2,0,00);
    if (counter<nrOfLines) {
      counter++;
      sl[counter]=new SpaceLines(counter,mouseX-width/2,mouseY,0);
    }
    rotY=0.0;
    rotateY(counter*smallRot);
    for (int i=1;i<=counter;i++) {
      pushMatrix();
      rotateY(-i*smallRot);
      sl[i].update();
      popMatrix();
    }
  }
}

void mousePressed() {
  go=true;
}

class SpaceLines {
  int nr;
  float x,y,z;
  float col;
  float[] lpx = new float[nrOfLines+1];
  float[] lpy = new float[nrOfLines+1];
  
  SpaceLines(int nr, float x, float y, float z) {
    this.nr=nr;
    this.x=x; this.y=y; this.z=z;
    col=0;
  }
  
  void update() {
    if (nr>15) {
      if (col<255) {col++;}
      stroke(col);
      line(x,y,z,sl[nr-15].x,sl[nr-15].y,sl[nr-15].z);
    }
  }
}
