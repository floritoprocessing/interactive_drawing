class QColor {
  
  private int r=0, g=0, b=0;
  
  QColor() {
  }
  
  QColor(int c8) {
    r = (c8>>16&0xFF)<<8;
    g = (c8>>8&0xFF)<<8;
    b = (c8&0xFF)<<8;
  }
  
  QColor(int _r, int _g, int _b) {
    r = _r;
    g = _g;
    b = _b;
    if (r<0) r=0;
    if (r>65535) r=65535;
    if (g<0) g=0;
    if (g>65535) g=65535;
    if (b<0) b=0;
    if (b>65535) b=65535;
    /*
    
    if (r>0xFFFF) r=0xFFFF;
    if (g<0) g=0;
    if (g>0xFFFF) g=0xFFFF;
    if (b<0) b=0;
    if (b>0xFFFF) b=0xFFFF;
    */
  }
  
  public void set(int c8) {
    r = (c8>>16&0xFF)<<8;
    g = (c8>>8&0xFF)<<8;
    b = (c8&0xFF)<<8;
  }
  
  public void set(QColor c) {
    r = c.red();
    g = c.green();
    b = c.blue();
  }
  
  public String toString() {
    return str(r)+" "+str(g)+" "+str(b);
  }
  
  public int red() { return r; }
  public int green() { return g; }
  public int blue() { return b; }
  
  public color toColor() {
    int r8 = r>>8&0xFF;
    int g8 = g>>8&0xFF;
    int b8 = b>>8&0xFF;
    return color(r8,g8,b8);
  }
}
