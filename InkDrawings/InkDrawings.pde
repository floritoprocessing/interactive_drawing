/*

Ink drawings

*/

import java.util.Vector;

Paper paper;
DropGroup drops;

void setup() {
  size(640,480,P3D);
  
  paper = new Paper(width,height);
  paper.setChannelDepth(Paper.CHANNEL_DEPTH_16BIT);
  paper.background(0xFFFFFF);
  
  drops = new DropGroup(width,height);
}

void draw() {
  if (mousePressed) {
    drops.addLineOfDrops(pmouseX,pmouseY,mouseX,mouseY);
  }
  
  drops.updateMov(paper);
  drops.updatePos();
  
  image(paper,0,0);
  drops.show();
}
