
class BLENDING_MODE {
  public static final int NORM = 0;
  public static final int ADD = 1;
  public static final int BRUISE = 2;
}


/*

QImage functions

*/

void setWithType(int type, int sx, int sy, color c, float p) {
  set(sx,sy,colorMixType(c,get(sx,sy),p,type));
}

void setWithType(int type, float x, float y, color c, float p) {
  int x0 = int(floor(x));
  int x1 = x0+1;
  int y0 = int(floor(y));
  int y1 = y0+1;
  float px1 = x-x0, px0 = 1.0-px1;
  float py1 = y-y0, py0 = 1.0-py1;
  float p00 = p*px0*py0;
  float p10 = p*px1*py0;
  float p01 = p*px0*py1;
  float p11 = p*px1*py1;
  set(x0,y0,colorMixType(c,get(x0,y0),p00,type));
  set(x1,y0,colorMixType(c,get(x1,y0),p10,type));
  set(x0,y1,colorMixType(c,get(x0,y1),p01,type));
  set(x1,y1,colorMixType(c,get(x1,y1),p11,type));
}


/*



*/


void setWithType(QImage img, int type, int sx, int sy, QColor c, float p) {
  if (img.getChannelDepth()==QImage.CHANNEL_DEPTH_8BIT) {
    img.set(sx,sy,colorMixType(c.toColor(),img.get(sx,sy),p,type));
  } else {
    img.set(sx,sy,colorMixType16bit(c,img.get16bit(sx,sy),p,type));
  }
}

void setWithType(QImage img, int type, float x, float y, QColor c, float p) {
  int x0 = int(floor(x));
  int x1 = x0+1;
  int y0 = int(floor(y));
  int y1 = y0+1;
  float px1 = x-x0, px0 = 1.0-px1;
  float py1 = y-y0, py0 = 1.0-py1;
  float p00 = p*px0*py0;
  float p10 = p*px1*py0;
  float p01 = p*px0*py1;
  float p11 = p*px1*py1;
  if (img.getChannelDepth()==QImage.CHANNEL_DEPTH_8BIT) {
    color co = c.toColor();
    img.set(x0,y0,colorMixType(co,img.get(x0,y0),p00,type));
    img.set(x1,y0,colorMixType(co,img.get(x1,y0),p10,type));
    img.set(x0,y1,colorMixType(co,img.get(x0,y1),p01,type));
    img.set(x1,y1,colorMixType(co,img.get(x1,y1),p11,type));
  } else {
    img.set(x0,y0,colorMixType16bit(c,img.get16bit(x0,y0),p00,type));
    img.set(x1,y0,colorMixType16bit(c,img.get16bit(x1,y0),p10,type));
    img.set(x0,y1,colorMixType16bit(c,img.get16bit(x0,y1),p01,type));
    img.set(x1,y1,colorMixType16bit(c,img.get16bit(x1,y1),p11,type));
  }
  
}




int colorMixType(int c1, int c2, float p1, int type) {

  int r1 = c1>>16&0xFF;
  int g1 = c1>>8&0xFF;
  int b1 = c1&0xFF;

  int r2 = c2>>16&0xFF;
  int g2 = c2>>8&0xFF;
  int b2 = c2&0xFF;  

  if (type==BLENDING_MODE.NORM) {
    float p2 = 1.0-p1;
    int rr = int(r1*p1+r2*p2);
    int gg = int(g1*p1+g2*p2);
    int bb = int(b1*p1+b2*p2);
    if (rr>255) rr=255;
    if (gg>255) gg=255;
    if (bb>255) bb=255;
    return (rr<<16|gg<<8|bb);
  } 
  else if (type==BLENDING_MODE.ADD) {
    int rr = r2 + int(r1*p1);
    int gg = g2 + int(g1*p1);
    int bb = b2 + int(b1*p1);
    if (rr>255) rr=255;
    if (gg>255) gg=255;
    if (bb>255) bb=255;
    return (rr<<16|gg<<8|bb);
  } 
  else if (type==BLENDING_MODE.BRUISE) {
    int rr = int(r2-(255-r1)*p1);
    int gg = int(g2-(255-g1)*p1);
    int bb = int(b2-(255-b1)*p1);
    if (rr<0) rr=0;
    if (gg<0) gg=0;
    if (bb<0) bb=0;
    return (rr<<16|gg<<8|bb);
  }

  return 0;
}


QColor colorMixType16bit(QColor c1, QColor c2, float p1, int type) {

  int r1 = c1.red();
  int g1 = c1.green();
  int b1 = c1.blue();

  int r2 = c2.red();
  int g2 = c2.green();
  int b2 = c2.blue(); 

  if (type==BLENDING_MODE.NORM) {
    float p2 = 1.0-p1;
    int rr = int(r1*p1+r2*p2);
    int gg = int(g1*p1+g2*p2);
    int bb = int(b1*p1+b2*p2);
    return new QColor(rr,gg,bb);
  } 
  else if (type==BLENDING_MODE.ADD) {
    int rr = r2 + int(r1*p1);
    int gg = g2 + int(g1*p1);
    int bb = b2 + int(b1*p1);
    return new QColor(rr,gg,bb);
  } 
  else if (type==BLENDING_MODE.BRUISE) {
    int rr = int(r2-(65535-r1)*p1);
    int gg = int(g2-(65535-g1)*p1);
    int bb = int(b2-(65535-b1)*p1);
    return new QColor(rr,gg,bb);
  }

  return new QColor();
}




/*
int colorMixNorm(int c1, int c2, float p1) {
 int r1 = c1>>16&0xFF;
 int g1 = c1>>8&0xFF;
 int b1 = c1&0xFF;
 int r2 = c2>>16&0xFF;
 int g2 = c2>>8&0xFF;
 int b2 = c2&0xFF;
 float p2 = 1.0-p1;
 
 int rr = int(r1*p1+r2*p2);
 int gg = int(g1*p1+g2*p2);
 int bb = int(b1*p1+b2*p2);
 
 return (rr<<16|gg<<8|bb);
 }
 
 int colorMixAdd(int c1, int c2, float p1) {
 int r1 = c1>>16&0xFF;
 int g1 = c1>>8&0xFF;
 int b1 = c1&0xFF;
 int r2 = c2>>16&0xFF;
 int g2 = c2>>8&0xFF;
 int b2 = c2&0xFF;
 float p2 = 1.0-p1;
 
 int rr = r2 + int(r1*p1);
 int gg = g2 + int(g1*p1);
 int bb = b2 + int(b1*p1);
 
 return (rr<<16|gg<<8|bb);
 }
 
 int colorMixBruise(int c1, int c2, float p1) {
 int r1 = 255 - (c1>>16&0xFF);
 int g1 = 255 - (c1>>8&0xFF);
 int b1 = 255 - (c1&0xFF);
 int r2 = c2>>16&0xFF;
 int g2 = c2>>8&0xFF;
 int b2 = c2&0xFF;
 float p2 = 1.0-p1;
 
 int rr = int(r2-r1*p1);
 int gg = int(g2-g1*p1);
 int bb = int(b2-b1*p1);
 if (rr<0) rr=0;
 if (gg<0) gg=0;
 if (bb<0) bb=0;
 
 return (rr<<16|gg<<8|bb);
 }
 *//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
