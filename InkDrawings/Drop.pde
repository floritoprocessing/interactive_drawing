class Drop {
  
  public static final int MAX_NEIGHBOUR_INFLUENCE_DISTANCE = 10;
  
  private float AVOIDANCE_FORCE = 1;
  private float MINIMUM_DISTANCE = 0.8;
  private float START_OFFSET = 1.00;
  
  private float MAX_ACCELERATION = 1.0;
  private float LIQUID_AMOUNT = 1.0;
  
  private float x, y;
  private float xm, ym;
  
  
  Drop(float _x, float _y) {
    float rd = random(TWO_PI);
    x = _x + START_OFFSET * cos(rd);
    y = _y + START_OFFSET * sin(rd);    
    xm = 0;
    ym = 0;
  }
  
  
  
  public float getX() {
    return x;
  }
  
  public float getY() {
    return y;
  }
  
  void updateMov(DropGroup all, Paper paper) {
    Vector neighbours = all.getNeighbours(this);
    float accX = 0 , accY = 0;
    for (int i=0;i<neighbours.size();i++) {
      Drop drop = ((Drop)neighbours.elementAt(i));
      float dx = drop.getX() - x;
      float dy = drop.getY() - y;
      float d = sqrt(sq(dx)+sq(dy));
      if (d<MAX_NEIGHBOUR_INFLUENCE_DISTANCE) {
        if (d<MINIMUM_DISTANCE) d=MINIMUM_DISTANCE;
        float F = AVOIDANCE_FORCE/(d*d);
        accX -= F*(dx/d);
        accY -= F*(dy/d);
      }
    }
    
    float acc = sqrt(sq(accX)+sq(accY));
    if (acc>MAX_ACCELERATION) {
      float p = MAX_ACCELERATION/acc;
      accX *= p;
      accY *= p;
    }
    
    xm += accX;
    ym += accY;
    
    float drag = paper.getDragAt(x,y);
    xm *= drag;
    ym *= drag;
  }
  
  
  
  void updatePos(DropGroup all) {
    x += xm;
    y += ym;
    if (x<0||x>=all.width||y<0||y>=all.height) all.remove(this);
  }
  
  
  
  void show() {
    noFill();
    stroke(0,0,0,255*LIQUID_AMOUNT);
    ellipseMode(RADIUS);
    ellipse(x,y,MAX_NEIGHBOUR_INFLUENCE_DISTANCE,MAX_NEIGHBOUR_INFLUENCE_DISTANCE);
    setWithType(BLENDING_MODE.NORM,x,y,0,LIQUID_AMOUNT);
  }
}
