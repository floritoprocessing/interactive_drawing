class DropGroup extends Vector {
  
  public int width, height;
  private DropSpace dropSpace;
  
  DropGroup(int w, int h) {
    super();
    width = w;
    height = h;
    dropSpace = new DropSpace(width,height,Drop.MAX_NEIGHBOUR_INFLUENCE_DISTANCE*1);
  }
  
  
  void addLineOfDrops(float x0, float y0, float x1, float y1) {
    float dx = x1-x0;
    float dy = y1-y0;
    float d = sqrt(sq(dx)+sq(dy));
    if (d>0) {
      for (float p=0;p<1;p+=1/d) {
        float x = x0+p*dx;
        float y = y0+p*dy;
        if (x>=0&&x<width&&y>=0&&y<height) {
          Drop drop = new Drop(x,y);
          add(drop);
          dropSpace.add(drop);
        }
      }
    } else {
      if (x0>=0&&x0<width&&y0>=0&&y0<height) {
        Drop drop = new Drop(x0,y0);
        add(drop);
        dropSpace.add(drop);
      }
    }
  }
  
  
  public Vector getNeighbours(Drop d) {
    return dropSpace.getNeighbours(d);
  }
  
  void updateMov(Paper paper) {
    for (int i=0;i<size();i++) ((Drop)elementAt(i)).updateMov(this,paper);
  }
  
  
  void updatePos() {
    dropSpace.clear();
    for (int i=0;i<size();i++) {
      Drop d = (Drop)elementAt(i);
      d.updatePos(this);
    }
    for (int i=0;i<size();i++) {
      Drop d = (Drop)elementAt(i);
      dropSpace.add(d);
    }
  }
  
  
  
  void show() {
    dropSpace.show();
    for (int i=0;i<size();i++) ((Drop)elementAt(i)).show();  
  }
}
