class QImage extends PImage {
  
  public static final int CHANNEL_DEPTH_8BIT = 0;
  public static final int CHANNEL_DEPTH_16BIT = 1;
  private int CHANNEL_DEPTH = 0;
  
  QColor pix[][];
  
  QImage(int w, int h) {
    super(w,h);
    pix = new QColor[w][h];
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) pix[x][y] = new QColor();
  }
  
  public void set(int x, int y, QColor c) {
    //println(c.toString());
    if (x<0) return;
    if (x>=width) return;
    if (y<0) return;
    if (y>=height) return;
    pix[x][y].set(c);
    this.set(x,y,c.toColor());
  }
  
  public QColor get16bit(int x, int y) {
    if (x<0) x=0;
    if (x>=width) x=width-1;
    if (y<0) y=0;
    if (y>=height) y=height-1;
    return pix[x][y];
  }
  
  public void background(int c) {
    QColor qc = new QColor(c);
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      pix[x][y].set(qc);
      this.set(x,y,c);
    }
  }
  
  public void setChannelDepth(int cd) {
    if (CHANNEL_DEPTH==CHANNEL_DEPTH_8BIT&&cd==CHANNEL_DEPTH_16BIT) {
      // convert to 16bit:
      for (int x=0;x<width;x++) for (int y=0;y<height;y++) pix[x][y].set(this.get(x,y));
      CHANNEL_DEPTH = cd;
    } else if (CHANNEL_DEPTH==CHANNEL_DEPTH_16BIT&&cd==CHANNEL_DEPTH_8BIT) {
      // convert to 8bit:
      for (int x=0;x<width;x++) for (int y=0;y<height;y++) this.set(x,y,pix[x][y].toColor());
      CHANNEL_DEPTH = cd;
    }
  }
  
  public int getChannelDepth() {
    return CHANNEL_DEPTH;
  }
  
  /*
  public void copyFromScreen(PApplet pa) {
    if (CHANNEL_DEPTH==CHANNEL_DEPTH_8BIT) {
      for (int x=0;x<width;x++) for (int y=0;y<height;y++) this.set(x,y,pa.get(x,y));
    } else if (CHANNEL_DEPTH==CHANNEL_DEPTH_16BIT) {
      for (int x=0;x<width;x++) for (int y=0;y<height;y++) pix[x][y] = new QColor(pa.get(x,y));
    }
  }
  */
  
}
