class DropSpace {
  
  int width, height;
  float screenToSpace;
  float size;
  
  Vector[][] space;
  
  DropSpace(int w, int h, int s) {
    width = w/s;
    height = h/s;
    size = s;
    screenToSpace = 1/(float)s;
    space = new Vector[width][height];
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) space[x][y] = new Vector();
  }
  
  public void add(Drop d) {
    int x = int(d.getX()*screenToSpace);
    int y = int(d.getY()*screenToSpace);
    space[x][y].add(d);
  }
  
  public Vector getNeighbours(Drop d) {
    Vector out = new Vector();
    int sx = int(d.getX()*screenToSpace);
    int sy = int(d.getY()*screenToSpace);
    for (int xo=-1;xo<=1;xo++) for (int yo=-1;yo<=1;yo++) {
      int x = sx + xo;
      int y = sy + yo;
      if (x>=0&&x<width&&y>=0&&y<width) {
        for (int i=0;i<space[x][y].size();i++) {
          if (((Drop)space[x][y].elementAt(i)) != d) out.add(space[x][y].elementAt(i));
        }
      }
    }
    return out;
  }
  
  
  public void clear() {
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) space[x][y].clear();
  }
  
  public void show() {
    stroke(0,0,0,128);
    rectMode(CORNER);
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      if (space[x][y].size()!=0) fill(255,0,0,128); else noFill(); 
      rect(x*size,y*size,size,size);
    }
  }
  
}
