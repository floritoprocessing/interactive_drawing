/*

goal is to create a routine that connects any amount of dots
as triangles to create a filling space

*/
import java.util.Vector;

PolySet polySet;


void setup() {
  size(500,300,P3D);
  polySet = new PolySet();
//  for (int i=0;i<2;i++)
    //polySet.add(new Dot(random(width),random(height)));
}



void draw() {
  if (mousePressed) {
    if (frameCount%3==0) polySet.add(new Dot(mouseX,mouseY));
  }
  
  polySet.moveDots();
  background(0);
  polySet.drawDots();
  stroke(255);
  polySet.connectDots();
  
  
}
