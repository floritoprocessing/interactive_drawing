class Dot {

  public float X=0, Y=0;
  public float XM=0, YM=0;
  public float DRAG = 0.98;
  private Vector CONNECTION = new Vector();
  
  Dot(float x, float y) {
    X = x;
    Y = y;
    float spd = random(0.3,1.4);
    float dir = random(TWO_PI);
    XM = spd*cos(dir);
    YM = spd*sin(dir);
  }
  
  Dot(float x, float y, float xm, float ym) {
    X = x;
    Y = y;
    XM = xm;
    YM = ym;
  }

  boolean move() {
    XM+=0.17;
    XM*=DRAG;
    YM*=DRAG;
    X+=XM;
    Y+=YM;
    if (X>=width) { 
      X-=width; 
      return false; 
    }
    else if (X<0) { 
      X+=width; 
      return false; 
    }
    if (Y>=height) { 
      Y-=height; 
      return false; 
    }
    if (Y<0) { 
      Y+=height; 
      return false; 
    }
    return true;
  }

  void draw() {
    ellipse(X,Y,5,5);
  }

}
