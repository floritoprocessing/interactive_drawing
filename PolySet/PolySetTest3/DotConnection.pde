class DotConnection {

  Dot D0, D1;
  float DISTANCE;

  DotConnection(DotConnection dc) {
    D0 = dc.D0;
    D1 = dc.D1;
    DISTANCE = dc.DISTANCE;
  }

  DotConnection(Dot d0, Dot d1, float distance) {
    D0 = d0;
    D1 = d1;
    DISTANCE = distance;
  }

  void setFrom(DotConnection dc) {
    D0 = dc.D0;
    D1 = dc.D1;
    DISTANCE = dc.DISTANCE;
  }

  void draw() {
    line(D0.X,D0.Y,D1.X,D1.Y);
  }

}



public boolean intersects(Dot d1, Dot d2, Dot d3, Dot d4) {
  float x1 = d1.X, y1 = d1.Y;
  float x2 = d2.X, y2 = d2.Y;
  float x3 = d3.X, y3 = d3.Y;
  float x4 = d4.X, y4 = d4.Y;
//  double x3 = P3.X, y3 = P3.Y;
//  double x4 = P4.X, y4 = P4.Y;
  float denom = (y4-y3)*(x2-x1) - (x4-x3)*(y2-y1);
  if (denom==0) return false;
  float numA = (x4-x3)*(y1-y3) - (y4-y3)*(x1-x3);
  float numB = (x2-x1)*(y1-y3) - (y2-y1)*(x1-x3);
  float uA = numA/denom;
  float uB = numB/denom;
  if (uA>=0&&uA<=1&&uB>=0&&uB<=1) {
    // INTERSECTION!
    //return new Vec(x1+uA*(x2-x1),y1+uA*(y2-y1));
    return true;
  }
  return false;
}



void bubbleSort(DotConnection[] array) {
  // START BUBBLE SORT:
  // -----------------

  float v1=0;
  float v2=0;
  int nBegin = 0;
  int nEnd=array.length-1;

  int lastSwap=0;
  int firstSwap=0;
  boolean swapped=true;
  boolean notYetSwapped=true;
  DotConnection temp = new DotConnection(array[0]);
  while (swapped) {
    swapped=false;
    lastSwap=0;
    notYetSwapped=true;
    for (int i=nBegin;i<nEnd;i++) {
      v1=array[i].DISTANCE;
      v2=array[i+1].DISTANCE;
      if (v1>v2) {
        
        //array[i]=v2;
        //array[i+1]=v1;
        temp.setFrom(array[i]);
        array[i].setFrom(array[i+1]);
        array[i+1].setFrom(temp);
        
        swapped=true;
        lastSwap=i;
        if (!swapped) {
          notYetSwapped=false;
          firstSwap=i;
        }
      }
    }
    nBegin = firstSwap;
    nEnd = lastSwap;//--;
  }

  // BUBBLE SORT END
  // ---------------
}
