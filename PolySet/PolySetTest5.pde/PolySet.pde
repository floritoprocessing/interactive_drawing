class PolySet extends Vector {

  PolySet() {
    super();
  }

  void connectDots(PImage img) {
    img.loadPixels();
    /*
    http://ieeexplore.ieee.org/iel5/8652/27421/01220458.pdf
     3.a Creating triangles in case of frame based video processing
     At first the two nearest nodes are found and connected. Then those two nearest nodes are
     found which fulfil the following conditions:
     - there is no connection between them
     - the new line between them doesn't cross any existing lines
     The method is repeated until all possible lines are found, creating finally the complete mesh.
     */

    // break all connections:
    for (int i=0; i<size(); i++) ((Dot)elementAt(i)).CONNECTION=new Vector();

    // make a list of all connections:
    int maxConnections = 0;
    for (int i=0; i<size()-1; i++) for (int j=i+1; j<size(); j++) maxConnections++;

    if (maxConnections==0) return;

    DotConnection[] allDotConnections = new DotConnection[maxConnections];
    int dc=0;
    for (int i=0; i<size()-1; i++) for (int j=i+1; j<size(); j++) {
      Dot d0 = (Dot)elementAt(i);
      Dot d1 = (Dot)elementAt(j);
      float d = sq(d0.X-d1.X)+sq(d0.Y-d1.Y);
      allDotConnections[dc]=new DotConnection(d0, d1, d);
      dc++;
    }

    // sort by distance:
    bubbleSort(allDotConnections);


    Vector drawConnection = new Vector();

    // At first the two nearest nodes are found and connected
    Dot d0 = allDotConnections[0].D0;
    Dot d1 = allDotConnections[0].D1;
    d0.CONNECTION.add(d1);
    d1.CONNECTION.add(d0);
    drawConnection.add(allDotConnections[0]);

    /*
    Then those two nearest nodes are
     found which fulfil the following conditions:
     - there is no connection between them
     - the new line between them doesn't cross any existing lines
     The method is repeated until all possible lines are found, creating finally the complete mesh.
     */

    for (int c=1; c<allDotConnections.length; c++) {
      d0 = allDotConnections[c].D0;
      d1 = allDotConnections[c].D1;
      boolean notConnected = !d0.CONNECTION.contains(d1);
      if (notConnected) {
        boolean crossLine = false;
        for (int c2=0; c2<drawConnection.size(); c2++) {
          Dot d2 = ((DotConnection)drawConnection.elementAt(c2)).D0;
          Dot d3 = ((DotConnection)drawConnection.elementAt(c2)).D1;
          boolean dotsNotEqual = !(d0.equals(d2)||d0.equals(d3)||d1.equals(d2)||d1.equals(d3));
          if (dotsNotEqual) {
            if (intersects(d0, d1, d2, d3)) {
              crossLine=true;
              c2=drawConnection.size();
            }
          }
        }
        if (!crossLine) {
          d0.CONNECTION.add(d1);
          d1.CONNECTION.add(d0);
          drawConnection.add(allDotConnections[c]);
        }
      }
    }

    //stroke(255,255,255);
    for (int i=0; i<drawConnection.size(); i++) {
      //stroke(255,255,255,32-31*(float)i/drawConnection.size());
      ((DotConnection)drawConnection.elementAt(i)).drawImg(img);
    }

    for (int i=0; i<drawConnection.size(); i++) {
      //stroke(255,255,255,32-31*(float)i/drawConnection.size());
      ((DotConnection)drawConnection.elementAt(i)).draw();
    }
    
    img.updatePixels();
  }


  void moveDots() {
    for (int i=0; i<size(); i++) {
      Dot d = (Dot)elementAt(i);
      if (!d.move()) {
        remove(d);
        i--;
      }
    }
  }


  void drawDots() {
    noFill();
    for (int i=0; i<size(); i++) {
      //if (i==0) stroke(255,0,0); else stroke(255,255,255);
      ((Dot)elementAt(i)).draw();
    }
  }
}
