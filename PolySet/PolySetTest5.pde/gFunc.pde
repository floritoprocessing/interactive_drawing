void softLineImg(PImage img, float x, float y, float x1, float y1, int r, int g, int b, int OPA) {
  float dx=x1-x;
  float dy=y1-y;
  float d = sqrt(sq(dx)+sq(dy));
  if (d>0) {
    for (float p=0;p<1.0;p+=1/d) {
      softSetImg(img,x+p*dx,y+p*dy,r,g,b,OPA);
    }
  }
}

void softSetImg(PImage img, int x, int y, int r, int g, int b, int OPA) {
  int oc = img.get(x,y);
  int or = oc>>16&0xFF;
  int og = oc>>8&0xFF;
  int ob = oc&0xFF;
  int tr = min(255,or+r);
  int tg = min(255,og+g);
  int tb = min(255,ob+b);
  int rr = (255*or + OPA*(tr-or))>>8;
  int gg = (255*og + OPA*(tg-og))>>8;
  int bb = (255*ob + OPA*(tb-ob))>>8;
  img.set(x,y,0xFF000000|rr<<16|gg<<8|bb);
}

void softSetImg(PImage img, float x, float y, int r, int g, int b, int OPA) {
  int x0 = (int)x, x1 = x0+1;
  int y0 = (int)y, y1 = y0+1;
  float px1 = x-x0, px0 = 1-px1;
  float py1 = y-y0, py0 = 1-py1;
  int op00 = (int)(px0*py0*OPA);
  int op01 = (int)(px0*py1*OPA);
  int op10 = (int)(px1*py0*OPA);
  int op11 = (int)(px1*py1*OPA);
  softSetImg(img,x0,y0,r,g,b,op00);
  softSetImg(img,x0,y1,r,g,b,op01);
  softSetImg(img,x1,y0,r,g,b,op10);
  softSetImg(img,x1,y1,r,g,b,op11);
}
