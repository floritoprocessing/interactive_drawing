/*

 goal is to create a routine that connects any amount of dots
 as triangles to create a filling space
 
 */

import java.util.Vector;

PolySet polySet;
Shot shot;
PImage img;

void setup() {
  size(500,300,P3D);
  img = new PImage(width,height);
  img.loadPixels();
  for (int i=0;i<img.pixels.length;i++) img.pixels[i]=0xFF000000;
  img.updatePixels();
  polySet = new PolySet();
  shot = null;
  //  for (int i=0;i<2;i++)
  //polySet.add(new Dot(random(width),random(height)));
  background(0);
}

void mousePressed() {
  shot = new Shot(random(width),height-1);
}
void mouseReleased() {
  shot=null;
}

void draw() {
  image(img,0,0);

  if (mousePressed) {
    if (frameCount%2==0) if (shot!=null) shot.addTo(polySet,mouseX,mouseY);
    
  }

  polySet.moveDots();
  polySet.connectDots(img);
  stroke(255,255,255);
  polySet.drawDots();

}
