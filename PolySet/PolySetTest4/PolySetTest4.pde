/*

goal is to create a routine that connects any amount of dots
as triangles to create a filling space

*/

import java.util.Vector;

PolySet polySet;
Shot shot;


void setup() {
  size(500,300,P3D);
  polySet = new PolySet();
  shot = null;
//  for (int i=0;i<2;i++)
    //polySet.add(new Dot(random(width),random(height)));
    background(0);
}

void mousePressed() {
  shot = new Shot(random(width),height-1);
}
void mouseReleased() {
  shot=null;
}

void draw() {
  
  
  if (mousePressed) {
    //if (frameCount%3==0) polySet.add(new Dot(mouseX,mouseY,2,-2));
    if (frameCount%3==0) if (shot!=null) shot.addTo(polySet,mouseX,mouseY);
  }
  
  polySet.moveDots();
  //background(0);
  //polySet.drawDots();
  polySet.connectDots();
  
  
}
