class Shot {
  
  int balls = 20;
  float baseX, baseY;
  float targetX, targetY;
  float FORCE = 0.05;
  
  Shot(float bx, float by) {
    baseX=bx;
    baseY=by;
  }
  
  void addTo(PolySet ps, float tx, float ty) {
    if (balls>0) {
      float bx = baseX+random(-5,5);
      float dx = FORCE*(tx-bx);
      float dy = FORCE*(ty-baseY);
      ps.add(new Dot(bx,baseY,dx,dy));
      balls--;
    }
  }
  
}
