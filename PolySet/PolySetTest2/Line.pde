class Line {
  
  float X0=0;
  float Y0=0;
  float X1=0;
  float Y1=0;
  
  Line(float x0, float y0, float x1, float y1) {
    X0=x0;
    Y0=y0;
    X1=x1;
    Y1=y1;
  }
  
  Line(Dot d0, Dot d1) {
    X0=d0.X;
    Y0=d0.Y;
    X1=d1.X;
    Y1=d1.Y;
  }
  
  void draw() {
    line(X0,Y0,X1,Y1);
  }
  

}
