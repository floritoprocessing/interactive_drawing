class PolySet extends Vector {

  PolySet() {
    super();
  }

  void drawNeighbours() {
    
    for (int i=0;i<size();i++) {
      
      Dot dot = (Dot)elementAt(i);
      // find the closest neighbours:
      int i0=-1, i1=-1;
      float minDist=sq(width)+sq(height);
      for (int j=0;j<size();j++) {
        if (i!=j) {
          Dot dotN = (Dot)elementAt(j);
          float dx=dot.X-dotN.X;
          float dy=dot.Y-dotN.Y;
          float d=dx*dx+dy*dy;
          if (d<minDist) {
            minDist = d;
            i1 = i0;
            i0 = j;
          }
        }
      }
      if (i0!=-1&&i1!=-1) {
      Dot dot0 = (Dot)elementAt(i0);
      Dot dot1 = (Dot)elementAt(i1);
      line(dot.X,dot.Y,dot0.X,dot0.Y);
      line(dot.X,dot.Y,dot1.X,dot1.Y);
      }
    }
  }


  void moveDots() {
    for (int i=0;i<size();i++)
      ((Dot)elementAt(i)).move();
  }


  void drawDots() {
    noFill();
    for (int i=0;i<size();i++) {
      if (i==0) stroke(255,0,0); else stroke(255,255,255);
      ((Dot)elementAt(i)).draw();
    }
  }

}
