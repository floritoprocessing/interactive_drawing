/*

goal is to create a routine that connects any amount of dots
as triangles to create a filling space

*/

import java.util.Vector;

PolySet polySet;


void setup() {
  size(400,300,P3D);
  polySet = new PolySet();
  for (int i=0;i<5;i++) 
    polySet.add(new Dot(random(width),random(height),random(-0.1,0.1),random(-0.1,0.1),i));
}

void mousePressed() {
  setup();
}

void draw() {
  
  
  background(0);
  polySet.drawDots();
  polySet.drawNeighbours();
  //polySet.moveDots();
  
}
