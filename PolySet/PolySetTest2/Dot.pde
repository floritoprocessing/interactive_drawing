class Dot {
  
  public float X=0, Y=0;
  public float XM=0, YM=0;
  private int index;
  
  Dot(float x, float y, float xm, float ym, int i) {
    X = x;
    Y = y;
    XM = xm;
    YM = ym;
    index = i;
  }
  
  void move() {
    X+=XM;
    Y+=YM;
    if (X>=width) X-=width;
    else if (X<0) X+=width;
    if (Y>=height) Y-=height;
    if (Y<0) Y+=height;
  }
  
  void draw() {
    ellipse(X,Y,5,5);
    //if (sqrt(sq(mouseX-X)+sq(mouseY-Y))<5) println("Dot "+index);
  }
  
}
