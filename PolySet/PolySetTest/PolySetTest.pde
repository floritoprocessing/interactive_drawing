/*

goal is to create a routine that connects any amount of dots
as triangles to create a filling space

*/

import java.util.Vector;


PolySet polySet;


void setup() {
  size(400,300,P3D);
  polySet = new PolySet(this);
  for (int i=0;i<500;i++) polySet.set(random(width),random(height));
  polySet.createLines();
}

void mousePressed() {
  setup();
}

void draw() {
  background(0);
  polySet.drawDots();
  polySet.drawLines();
  
  polySet.clearLines();
  polySet.clearSpaceGrid();
  polySet.moveDots();
  polySet.createLines();
}
