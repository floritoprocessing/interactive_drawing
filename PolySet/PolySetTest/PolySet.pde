class PolySet extends Vector {
  
  int MIN_SIDE_LENGTH = 2;
  int min_side_length_sq = MIN_SIDE_LENGTH*MIN_SIDE_LENGTH;
  int MAX_SIDE_LENGTH = 30;
  int max_side_length_sq = MAX_SIDE_LENGTH*MAX_SIDE_LENGTH;
  
  Vector[][] spaceGrid;
  Vector lines;
  
  PApplet pa;
  
  PolySet(PApplet _pa) {
    super();
    pa = _pa;
    spaceGrid = new Vector[1+pa.width/MAX_SIDE_LENGTH][1+pa.height/MAX_SIDE_LENGTH];
    for (int i=0;i<spaceGrid.length;i++) for (int j=0;j<spaceGrid[i].length;j++) spaceGrid[i][j]=new Vector();
    lines = new Vector();
  }
  
  void set(float x, float y) {
    Dot d = new Dot(x,y,MAX_SIDE_LENGTH,random(-0.5,0.5),random(-0.5,0.5));
    add(d);
    spaceGrid[d.SCALED_X][d.SCALED_Y].add(d);
  }
  
  void moveDots() {
    for (int i=0;i<size();i++) {
      Dot d = (Dot)elementAt(i);
      d.move();
      spaceGrid[d.SCALED_X][d.SCALED_Y].add(d);
    }
  }
  
  void createLines() {
    for (int i=0;i<size();i++) {
      Dot dot = (Dot)elementAt(i);
      
      // find neighbours;
      Vector neighbours = new Vector();
      for (int xo=-1;xo<=1;xo++) for (int yo=-1;yo<=1;yo++) {
        int ix = dot.SCALED_X+xo;
        int iy = dot.SCALED_Y+yo;
        if (ix>=0&&ix<spaceGrid.length&&iy>=0&&iy<spaceGrid[0].length) {
          for (int n=0;n<spaceGrid[ix][iy].size();n++) {
            Dot dn = (Dot)spaceGrid[ix][iy].elementAt(n);
            float dx = dot.X-dn.X;
            float dy = dot.Y-dn.Y;
            float dSq = sq(dx)+sq(dy);
            if (dSq>min_side_length_sq&&dSq<max_side_length_sq)
              neighbours.add(dn);
          }
//          neighbours.addAll(spaceGrid[ix][iy]);
        }
      }
      
      for (int n=0;n<neighbours.size();n++) {
        Dot dn = (Dot)neighbours.elementAt(n);
        Line li = new Line(dot.X,dot.Y,dn.X,dn.Y);
        lines.add(li);
      }
      
    }
    
  }
  
  void clearLines() {
    lines.clear();
  }
  
  void clearSpaceGrid() {
    for (int i=0;i<spaceGrid.length;i++) for (int j=0;j<spaceGrid[i].length;j++) spaceGrid[i][j].clear();
  }
  
  void drawDots() {
    noFill();
    stroke(255,255,255);
    for (int i=0;i<size();i++)
      ((Dot)elementAt(i)).draw();
  }
  
  void drawLines() {
    stroke(255,255,255,128);
    for (int i=0;i<lines.size();i++)
      ((Line)lines.elementAt(i)).draw();
  }
  
}
