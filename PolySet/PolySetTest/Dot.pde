class Dot {
  
  public float X=0, Y=0;
  public float XM=0, YM=0;
  public int SCALED_X=0, SCALED_Y=0;
  public float DIVIDER=1.0;
  
  public Vector CONNECTIONS = new Vector();
  
  Dot(float x, float y, float divider, float xm, float ym) {
    X = x;
    Y = y;
    DIVIDER = divider;
    SCALED_X=(int)(X/DIVIDER);
    SCALED_Y=(int)(Y/DIVIDER);
    XM = xm;
    YM = ym;
  }
  
  void move() {
    X+=XM;
    Y+=YM;
    if (X>=width) X-=width;
    else if (X<0) X+=width;
    if (Y>=height) Y-=height;
    if (Y<0) Y+=height;
    SCALED_X=(int)(X/DIVIDER);
    SCALED_Y=(int)(Y/DIVIDER);
    CONNECTIONS.clear();
  }
  
  void draw() {
    ellipse(X,Y,5,5);
  }
  
}
