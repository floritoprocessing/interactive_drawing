class Relief extends GrafImage {
  
  float ALTITUDE[][];
  
  Relief(int w, int h, int bm) {
    super(w,h,bm);
    PImage img = new PImage(w,h);
    ALTITUDE = new float[w][h];
    img = loadImage("relief1.jpg");
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      int c = img.get(x,y);
      set(x,y,c);
      ALTITUDE[x][y] = ((c>>16&0xFF)+(c>>8&0xFF)+(c&0xFF))/765.0;
    }
  }
  
  float altitudeAt(Vec pos) {
    float x = (float)pos.x;
    float y = (float)pos.y;
    if (x<0) x=0;
    if (x>=width) x=width-1;
    if (y<0) y=0;
    if (y>=height) y=height-1;
    return ALTITUDE[(int)x][(int)y];
  }
  
}
