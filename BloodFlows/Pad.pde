class Pad {
  
  boolean ACTIVE = false;
  Vec BLOOD_POINT;
  float xMin, xRange;
  float yMin, yRange;
  
  Pad(float x0, float y0, float xr, float yr) {
    xMin = x0;
    xRange = xr;
    yMin = y0;
    yRange = yr;
    newBloodPoint();
  }
  
  Vec newBloodPoint() {
    BLOOD_POINT = new Vec( xMin + random(xRange) , yMin + random(yRange) );
    return BLOOD_POINT;
  }
  
}
