class Drop {
  
  int COLOR = 0x501000;
  float OPACITY = 0.5;
  float SPEED = 1.5;
  float FRICTION = 0.98;
  
  Vec pos;
  Vec mov;
  
  Drop(Vec _pos) {
    pos = new Vec(_pos);
    mov = new Vec(SPEED,0,0);
    mov.rotZ(random(TWO_PI));
  }
  
  void move(Relief r) {
    float altHere = r.altitudeAt(pos);
    for (int xo=-1;xo<=1;xo++) for (int yo=-1;yo<=1;yo++) {
      float altThere = r.altitudeAt(vecAdd(pos,new Vec(xo,yo)));
      float aHoriz = altThere-altHere;
      mov.add(vecMul(new Vec(xo,yo,0),-aHoriz));
    }
    
    pos.add(mov);
    mov.mul(FRICTION);
  }
  
}
