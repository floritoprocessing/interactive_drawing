import Graf.*;
import java.util.Vector;

int MATRIX_WIDTH = 10, MATRIX_HEIGHT = 10;

GrafUtil gu;
GrafImage FLOOR;
Matrix m;
Relief r;
Drops DROPS;


void setup() {
  size(600,600,P3D);
  
  gu = new GrafUtil(this,GrafUtil.BLEND_MODE_NORMAL);
  FLOOR = new GrafImage(width,height,GrafUtil.BLEND_MODE_NORMAL);
  
  m = new Matrix(MATRIX_WIDTH,MATRIX_HEIGHT,width,height);
  r = new Relief(width,height,GrafUtil.BLEND_MODE_NORMAL);
  
  DROPS = new Drops();
  
  DROPS.linkMatrix(m);
  DROPS.linkRelief(r);
  
}

void draw() {
  m.listenToMousePressed();
  DROPS.create();
  background(128);
  //image(FLOOR,0,0);
  //image(r,0,0);
  m.show(gu);
  DROPS.show(gu);
  
  DROPS.move();
  m.reset();
}
