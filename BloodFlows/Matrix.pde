class Matrix {
  
  int width, height;
  int screenWidth, screenHeight;
  float padWidth, padHeight;
  Pad pad[][];
  
  Matrix(int w, int h, int sw, int sh) {
    width = w;
    height = h;
    screenWidth = sw;
    screenHeight = sh;
    pad = new Pad[width][height];
    padWidth = (float)screenWidth/width;
    padHeight = (float)screenHeight/height;
    for (int x=0;x<pad.length;x++) for (int y=0;y<pad[x].length;y++) pad[x][y]=new Pad(padX_to_screenX(x),padY_to_screenY(y),padWidth,padHeight);
  }
  
  int screenX_to_padX(float sx) {
    if (sx<0) sx=0;
    if (sx>=screenWidth) sx=screenWidth-1;
    return (int)(sx/padWidth);
  }
  int screenY_to_padY(float sy) {
    if (sy<0) sy=0;
    if (sy>=screenHeight) sy=screenHeight-1;
    return (int)(sy/padHeight);
  }
  
  int padX_to_screenX(int px) {
    return (int)(px*padWidth);
  }
  int padY_to_screenY(int py) {
    return (int)(py*padHeight);
  }
  
  
  Vector getActivePads() {
    Vector out = new Vector();
    for (int x=0;x<pad.length;x++) for (int y=0;y<pad[x].length;y++) if (pad[x][y].ACTIVE) out.add(pad[x][y]);
    return out;
  }
  
  void listenToMousePressed() {
    if (mousePressed) {
      pad[screenX_to_padX(mouseX)][screenY_to_padY(mouseY)].ACTIVE=true;
    }
  }
  
  void show(GrafUtil gu) {
    for (int x=0;x<pad.length;x++) for (int y=0;y<pad[x].length;y++) {
      gu.circle((x+.5)*padWidth,(y+.5)*padHeight,(padWidth<padHeight?padWidth:padHeight)/2.0,pad[x][y].ACTIVE?0xFF0000:0x000000,pad[x][y].ACTIVE?.7:.3);
    }
  }
  
  void reset() {
    for (int x=0;x<pad.length;x++) for (int y=0;y<pad[x].length;y++) pad[x][y].ACTIVE=false;
  }
  
}
