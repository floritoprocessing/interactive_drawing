class Drops extends Vector {
  
  Matrix matrix;
  Relief relief;
  
  Drops() {
    super();
  }
  
  void linkMatrix(Matrix _m) {
    matrix = _m;
  }
  
  void linkRelief(Relief _r) {
    relief = _r;
  }
  
  void create() {
    Vector pads = matrix.getActivePads();
    for (int i=0;i<pads.size();i++) {
      Vec bloodPoint = ((Pad)pads.elementAt(i)).newBloodPoint();
      add(new Drop(bloodPoint));
    }
  }
  
  void move() {
    for (int i=0;i<size();i++) ((Drop)elementAt(i)).move(relief);
  }
  
  void show(GrafUtil gu) {
    for (int i=0;i<size();i++) {
      gu.circle(((Drop)elementAt(i)).pos.x,((Drop)elementAt(i)).pos.y,5,0x501000,.3);
    }
  }
  
}
