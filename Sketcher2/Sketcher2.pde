Sketcher[] sk; int nrOfSk=100000, actualSk;
int drawColor=0;
int drawPhase;

// variables used for calculating average center
float br, avx, avy, div, tot_avx, tot_avy;
int tot_x=0,tot_y=0;

void setup() {
  size(800,600);
  background(255); //framerate(50);
  sk=new Sketcher[nrOfSk];
  initSketchers();
  drawPhase=1;
}

void initSketchers() {
  for (int i=0;i<nrOfSk;i++) { sk[i]=new Sketcher(width,height); sk[i].init(); }
  actualSk=0;
}

void draw() {
  // DRAW SKETCHERS -------------------------------------------------------
  if (actualSk<nrOfSk-1&&mousePressed) {
    actualSk++;
    sk[actualSk].clone(sk[actualSk-1]);
    for (int i=0;i<25;i++) {sk[actualSk].makeLine(0);}
  }
  if (!mousePressed) {sk[actualSk].cen.x=mouseX;sk[actualSk].cen.y=mouseY;}
}


class Sketcher {
  int w,h;    //max width/height;
  Vec cen=new Vec(), rad=new Vec();
  float rd1,rd2,rdr;
  
  Sketcher(int limX, int limY) {
    w=limX; h=limY;
  }
  
  void init() {
     cen.init(random(w),random(h));
     rad.init(random(w/80.0,w/12.0),random(h/80.0,h/12.0));
     rd1=random(TWO_PI); rd2=rd1+random(PI/16.0,PI); rdr=abs(rd1-rd2);
  }
  
  void clone(Sketcher isk) {
    cen.init((15*isk.cen.x+mouseX)/16.0,(15*isk.cen.y+mouseY)/16.0);
    rad.init(isk.rad.x+random(-w/100.0,w/100.0),isk.rad.y+random(-h/100.0,h/100.0));
    rd1=random(TWO_PI); rd2=rd1+random(PI/16.0,PI); rdr=abs(rd1-rd2);
  }
  
  void makeLine(int c) {
    float trd1=rd1+random(-rdr/4.0,rdr/4.0);
    float trd2=rd2+random(-rdr/4.0,rdr/4.0);
    float tcx=cen.x+random(-w/150.0,w/150.0), tcy=cen.y+random(-h/150.0,h/150.0);
    for (float rd=trd1;rd<=trd2;rd+=TWO_PI/720.0) {
      float sx=tcx+rad.x*cos(rd), sy=tcy+rad.y*sin(rd);
      softset(int(sx),int(sy),c,0.01);
    }
  }
}


class Vec {
  float x,y;
  Vec() {}
  void init(float ix, float iy) {x=ix;y=iy;}
}

void softset(int x, int y, int c1, float p1) {
  float p2=1.0-p1;
  int c2=get(x,y);
  int r=int(p1*(c1>>16&255)+p2*(c2>>16&255));
  int g=int(p1*(c1>>8&255)+p2*(c2>>8&255));
  int b=int(p1*(c1&255)+p2*(c2&255));
  set(x,y,(255<<24|r<<16|g<<8|b));
}
