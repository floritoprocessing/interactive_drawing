class ExternalInput {

  // mode holds the status of input modes, looks like this: B 000;
  // bit 0 for mouse on/off, bit 1 for keyboard on/off, bit 2 for serial on/off
  public static final int MODE_MOUSE = 0;    // bit 0 of mode
  public static final int MODE_KEYBOARD = 1; // bit 1 of mode
  public static final int MODE_SERIAL = 2;   // bit 2 of mode
  private String[] MODE_NAME = { 
    "Mouse","Keyboard","Serial"           };
  private int mode = MODE_MOUSE;


  // reference to parent applet
  private PApplet pa;          


  // input Matrix definition:
  // the Matrix holds values between 0 and VALUE_MAX
  private int width, height;
  private float[][] inputMatrix;
  private static final float VALUE_MAX = 1024;


  // size of the inputMatrix box size screen representation
  private float inputMatrix_screenRectWidth;
  private float inputMatrix_screenRectHeight;





  /*
   ExternalInput CONSTRUCTOR:
   sets reference to PApplet
   set width/height (input matrix)
   resets input matrix array
   set size of input matrix screen representation
   */

  ExternalInput(PApplet _pa, int w, int h) {
    pa = _pa;
    width = w;
    height = h;
    inputMatrix = new float[width][height];
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) inputMatrix[x][y] = 0;
    inputMatrix_screenRectWidth = pa.width/(float)width;
    inputMatrix_screenRectHeight = pa.height/(float)height;
  }






  /*
    Private Methods for setting / reading inputMatrix array. Has security levels built in
   */

  private void setInputMatrix(int x, int y, float val) {
    if (x>=0&&x<width&&y>=0&&y<height&&val>=0&&val<=VALUE_MAX) {
      inputMatrix[x][y] = val;
    }
  }
  private float getInputMatrix(int x, int y) {
    if (x>=0&&x<width&&y>=0&&y<height) return inputMatrix[x][y]; 
    else return -1;
  }





  /*
    Private Method for converting screen input to inputMatrix
   */
  private float screenToMatrixX(float x) {
    return width*(x/(float)pa.width);
  }
  private float screenToMatrixY(float y) {
    return height*(y/(float)pa.height);
  }


  /*
    Method for converting inputMatrix to screen input
   */
  private float matrixToScreenX(float x) {
    return pa.width*x/(float)width;
  }
  private float matrixToScreenY(float y) {
    return pa.height*y/(float)height;
  }






  /*
    Public Methods for reading width/height, matrixRectWidth/Height
   */

  public int getWidth() {
    return width;
  }
  public int getHeight() {
    return height;
  }
  public float getScreenRectWidth() {
    return inputMatrix_screenRectWidth;
  }
  public float getScreenRectHeight() {
    return inputMatrix_screenRectHeight;
  }




  /*
    Public Method for returning level of External input. Returns values from 0..1
   */
   
   public float getPressureAt(int x, int y) {
     return getInputMatrix(x,y)/VALUE_MAX;
   }





  /*
    GET EXTERNAL INPUT
   Public Main Method for getting either mouse/keyboard or serial input
   */

  public void getInput() {


    // REACT TO MOUSE INPUT:
    // ---------------------
    if ((mode>>MODE_MOUSE&1)==1) {      

      // mouse: smooth drawing:
      if (mousePressed) {
        int mx = mouseX;
        int my = mouseY;
        if (mx<0) mx=0;
        if (mx>=pa.width) mx=pa.width-1;
        if (my<0) my=0;
        if (my>=pa.height) my=pa.height-1;
        float x = screenToMatrixX(mx) - 0.5;
        float y = screenToMatrixY(my) - 0.5;
        float setLevel = 100;
        Vector xyp = getAntiAliasXYP(x,y);
        for (int i=0;i<2;i++) for (int j=0;j<2;j++) {
          int xi = ((int[])xyp.elementAt(0))[i];
          int yi = ((int[])xyp.elementAt(1))[j];
          float pp = ((float[][])xyp.elementAt(2))[i][j];
          setInputMatrix(xi,yi,getInputMatrix(xi,yi)+setLevel*pp);
        }  
      }

      // mouse: and a little fade out of whole input matrix;      
      for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
        float fadeLevel = 0.8;
        setInputMatrix(x,y,getInputMatrix(x,y) * fadeLevel);
      }
    }



    // REACT TO KEYBOARD INPUT:
    // ------------------------
    if ((mode>>MODE_KEYBOARD&1)==1) {


    }




    // REACT TO SERIAL INPUT:
    if ((mode>>MODE_SERIAL&1)==1) {


    }

  }






  /*
    toggles Input Mode
   (mouse_input / keyboard_input / serial_input)
   */

  public void toggleMode(int m) {
    int newMode = 0;
    println("Input:");
    for (int i=0;i<3;i++) {
      int bit = (i==m) ? (1 - (mode>>i&1)):(mode>>i&1);
      newMode = newMode | (bit<<i);
      println(MODE_NAME[i]+":\t"+(bit==1?"ON":"OFF"));
    }
    println();
    mode = newMode;
  }





  /*
  Draw the inputMatrix on Screen.
   Will be displayed as semi-transparent rectangles
   */

  public void toScreen() {
    rectMode(CORNER);
    for (int x=0;x<width;x++) {
      for (int y=0;y<height;y++) {
        float br = inputMatrix[x][y]/8.0; // 0..128
        stroke(255,0,0,64+br);
        fill(72,255,0,br);
        rect(x*inputMatrix_screenRectWidth+1,y*inputMatrix_screenRectHeight+1,inputMatrix_screenRectWidth-2,inputMatrix_screenRectHeight-2);
      }
    }
  }

}
