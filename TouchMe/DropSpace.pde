class DropSpace {
  
  
  private int width, height;
  private float spacing;
  private Vector[][] dropOccupation;
  
  
  DropSpace(PImage img, float _spacing) {
    width = int(img.width/_spacing);
    height = int(img.height/_spacing);
    spacing = _spacing;
    initDropOccupation();
  }
  
  
  
  private void initDropOccupation() {
    dropOccupation = new Vector[width][height];
    for (int i=0;i<width;i++) for (int j=0;j<height;j++) dropOccupation[i][j] = new Vector();
  }
  
  
  public void clear() {
    initDropOccupation();
  }
  
  
  
  public void add(Drop d) {
    float x = d.getX();
    float y = d.getY();
    int xi = int(x/spacing);
    int yi = int(y/spacing);
    if (xi>=0&&xi<width&&yi>=0&&yi<height) {
      dropOccupation[xi][yi].add(d);
    }
  }
  
  
  
  public Vector getNeighbours(Drop d) {
    float x = d.getX();
    float y = d.getY();
    int xi = int(x/spacing);
    int yi = int(y/spacing);
    Vector out = new Vector();
    for (int xo=-1;xo<=1;xo++) for (int yo=-1;yo<=1;yo++) {
      int xn = xi+xo;
      int yn = yi+yo;
      if (xn>=0&&xn<width&&yn>=0&&yn<height) {
        for (int i=0;i<dropOccupation[xn][yn].size();i++) {
          out.add(dropOccupation[xn][yn].elementAt(i));
        }
      }
    }
    return out;
  }
  
  
  
  public void drawOn(PImage img) {
    for (int x=0;x<width;x++) for (int y=0;y<height*spacing;y++) {
      img.set(int(x*spacing),y,color(192,192,255));
    }
    for (int y=0;y<height;y++) for (int x=0;x<width*spacing;x++) {
      img.set(x,int(y*spacing),color(192,192,255));
    }
  }
  
}
