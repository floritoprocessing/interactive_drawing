Vector getAntiAliasXYP(float x, float y) {
  
  Vector out = new Vector();
  
  int[] XX = new int[2];
  int[] YY = new int[2];
  float[][] PPPP = new float[2][2];
  
  XX[0] = int(floor(x));
  XX[1] = XX[0] + 1;
  out.add(XX);
  
  YY[0] = int(floor(y));
  YY[1] = YY[0] + 1;
  out.add(YY);
  
  float px1 = x-XX[0];
  float px0 = 1-px1;
  float py1 = y-YY[0];
  float py0 = 1-py1;
  PPPP[0][0] = px0*py0;
  PPPP[0][1] = px0*py1;
  PPPP[1][0] = px1*py0;
  PPPP[1][1] = px1*py1;
  out.add(PPPP);
  
  return out;
}
