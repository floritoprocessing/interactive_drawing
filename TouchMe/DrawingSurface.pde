class DrawingSurface extends PImage {
  
  private static final boolean DROP_SPACE_VISIBLE = false;
  private static final int DROP_DRAWING_INTENSITY = 30;
  
  public static final int DRAW_COLOR_RED =  0x500000;
  public static final int DRAW_COLOR_GREEN =  0x005000;
  public static final int DRAW_COLOR_BLUE =  0x000050;
  public static final int DRAW_COLOR_YELLOW = 0xd8d800;
  
  private Vector drops;
  private DropSpace dropSpace;
  private float[][] paperSaturation;
  private int drawColor = DRAW_COLOR_BLUE;
  
  
  DrawingSurface(int w, int h) {
    super(w,h);
    loadPixels();
    for (int i=0;i<pixels.length;i++) pixels[i]=0xFFFFFF;
    updatePixels();
    drops = new Vector();
    dropSpace = new DropSpace(this,Drop.INFLUENCE_DISTANCE);
    paperSaturation = new float[w][h];
    for (int i=0;i<w;i++) for (int j=0;j<h;j++) paperSaturation[i][j] = 0.0;
    
  }
  
  
  
  public void setDrawColor(int c) {
    drawColor = c;
  }
  
  public float getPaperSaturation(float x, float y) {
    int sx=int(x);
    int sy=int(y);
    if (sx<0) sx=0;
    if (sx>=width) sx=width-1;
    if (sy<0) sy=0;
    if (sy>=height) sy=height-1;
    return paperSaturation[sx][sy];
  }
  
  public void addPaperSaturation(float x, float y, float p) {
    int sx=int(x);
    int sy=int(y);
    if (sx<0) sx=0;
    if (sx>=width) sx=width-1;
    if (sy<0) sy=0;
    if (sy>=height) sy=height-1;
    paperSaturation[sx][sy] += p;
  }
  /*
  public float getDarknessAt(float x, float y) {
    int c = this.get(int(x),int(y));
    float dark = 1.0 - (((c>>16&0xFF) + (c>>8&0xFF) + (c&0xFF)) / 765.0);
    return dark;
  }
  */
  
  
  
  
  public void removeDrop(Drop d) {
    drops.remove(d);
  }
  
  /*
    Method to read ExternalInput and create new drops
  */
  public void updateFrom(ExternalInput ei) {
    
    // transform input to new drops:
    float totalPressure = 0;
    for (int i=0;i<ei.getWidth();i++) for (int j=0;j<ei.getHeight();j++) {
      float pressure = ei.getPressureAt(i,j);
      totalPressure++;
    }
    
    
    // create drops
    for (int i=0;i<totalPressure*DROP_DRAWING_INTENSITY;i++) {
      int x = (int)random(width);
      int y = (int)random(height);
      
      float pressure = ei.getPressureAt((int)ei.screenToMatrixX(x),(int)ei.screenToMatrixY(y));
      if (random(1.0)<pressure) {
        drops.add(new Drop(x,y,drawColor));
      }
    }
    
    
    // update their position    
    dropSpace.clear();
    for (int i=0;i<drops.size();i++) {
      ((Drop)drops.elementAt(i)).updatePosition(this,dropSpace);
    }
    
    // update movement:
    // neighbours and gravity:
    for (int i=0;i<drops.size();i++) {
      ((Drop)drops.elementAt(i)).updateMovement(this,dropSpace);
    }


    // draw them
    //for (int i=0;i<pixels.length;i++) pixels[i]=0xFFFFFF;
    if (DROP_SPACE_VISIBLE) dropSpace.drawOn(this);
    for (int i=0;i<drops.size();i++) {
      ((Drop)drops.elementAt(i)).drawOn(this);
    }
    
    updatePixels();
  }
  
  
  
  public void showDrops() {
    stroke(0,0,0,64);
    for (int i=0;i<drops.size();i++) {
      ((Drop)drops.elementAt(i)).toScreen();
    }
  }
  
  
  /*
  void bleachAndEvaporate() {
    for (int i=0;i<this.pixels.length;i++) {
      float rr = this.pixels[i]>>16&0xFF;
      float gg = this.pixels[i]>>8&0xFF;
      float bb = this.pixels[i]&0xFF;
      float average = (rr+gg+bb)/3.0;
      if (rr<average) rr+=0.5; else if (rr>average) rr-=0.5;
      if (gg<average) gg+=0.5; else if (gg>average) gg-=0.5;
      if (bb<average) bb+=0.5; else if (bb>average) bb-=0.5;
      rr+=0.9;
      gg+=0.9;
      bb+=0.9;
      this.pixels[i] = int(rr)<<16|int(gg)<<8|int(bb);
    }
  }
  */
  
  
  
  
}
