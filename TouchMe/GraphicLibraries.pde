
int colorMix(int c0, int c1, float p0) {
  float p1 = 1.0 - p0;
  int r = int( p0*(c0>>16&0xFF) + p1*(c1>>16&0xFF) );
  int g = int( p0*(c0>>8&0xFF)  + p1*(c1>>8&0xFF)  );
  int b = int( p0*(c0&0xFF)     + p1*(c1&0xFF)     );
  return r<<16|g<<8|b;
}

void softSetOn(float x, float y, int col, float p, PImage img) {
  img.set(int(x),int(y),colorMix(col,img.get(int(x),int(y)),p));
}
