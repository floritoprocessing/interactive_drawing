import java.util.Vector;

ExternalInput externalIn;
DrawingSurface drawingSurface;

boolean SHOW_AGENTS;

void setup() {
  size(640,480,P3D);
  
  externalIn = new ExternalInput(this,40,30);
  externalIn.toggleMode(ExternalInput.MODE_MOUSE);
  
  drawingSurface = new DrawingSurface(640,480);
  
  SHOW_AGENTS = false;
}

void keyPressed() {
  if (key=='1') externalIn.toggleMode(ExternalInput.MODE_MOUSE);
  //if (key=='2') externalIn.toggleMode(ExternalInput.MODE_KEYBOARD);
  //if (key=='3') externalIn.toggleMode(ExternalInput.MODE_SERIAL);
  
  if (key=='r') drawingSurface.setDrawColor(DrawingSurface.DRAW_COLOR_RED);
  if (key=='g') drawingSurface.setDrawColor(DrawingSurface.DRAW_COLOR_GREEN);
  if (key=='b') drawingSurface.setDrawColor(DrawingSurface.DRAW_COLOR_BLUE);
  if (key=='y') drawingSurface.setDrawColor(DrawingSurface.DRAW_COLOR_YELLOW);
  
  if (key=='s') SHOW_AGENTS = !SHOW_AGENTS;
}

void draw() {
  externalIn.getInput();
  drawingSurface.updateFrom(externalIn);
  image(drawingSurface,0,0);
  if (SHOW_AGENTS) drawingSurface.showDrops();
  //externalIn.toScreen();
}
