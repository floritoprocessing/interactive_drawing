class Drop {

  private static final float FRICTION = 1.0;

  private static final float FRICTION_NEIGHBOUR_DECREASE_MAX = 0.3;
  private static final int FRICTION_NEIGHBOUR_AMOUNT_MIN = 10;
  private static final int FRICTION_NEIGHBOUR_AMOUNT_MAX = 20;

  private static final float FRICTION_SATURATION_MIN = 0.1;
  private static final float FRICTION_REDUCTION_FROM_SATURATION_MAX = 0.25;

  private static final float GRAVITY_STRENGTH = 1.0;

  private static final float DRAW_RADIUS = 3.0;

  public static final float INFLUENCE_DISTANCE = 8.0;
  public static final float INFLUENCE_STRENGTH = 3.0;
  public static final float INFLUENCE_MAX_STRENGTH = 0.5;
  public static final float INFLUENCE_MOVEMENT = 0.05;

  public static final float DROP_FLUID_LOSS_MAX = 0.02;
  public static final float DROP_DRAWING_STRENGTH_MAX = 0.1;

  private int DRAW_COLOR = 0x000050;
  private float x, y;
  private float xm, ym;
  private boolean gravity = true;
  private int numberOfNeighbours = 0;
  private float fluidAmount=1.0;



  Drop(float _x, float _y) {
    x = _x;
    y = _y;
    xm = 0;
    ym = 0;
  }
  
  Drop(float _x, float _y, int dc) {
    this(_x,_y);
    DRAW_COLOR = dc;
  }




  public float getX() {
    return x;
  }
  public float getY() {
    return y;
  }

  public float getMovX() {
    return xm;
  }
  public float getMovY() {
    return ym;
  }

  public void accX(float _ax) {
    xm+=_ax;
  }
  public void accY(float _ay) {
    ym+=_ay;
  }



  public void updatePosition(DrawingSurface ds, DropSpace dSpace) {
    x += xm;
    y += ym;
    if (x<0||x>=drawingSurface.width||y<0||y>drawingSurface.height) {
      drawingSurface.removeDrop(this); 
    } 
    else {
      dSpace.add(this);
    }
  }




  public void updateMovement(DrawingSurface ds, DropSpace dSpace) {

    // FIND ALL NEIGHBOURS:
    Vector neighbours = dSpace.getNeighbours(this);
    if (neighbours.size()>0) {  
      for (int i=0;i<neighbours.size();i++) {
        if (neighbours.elementAt(i)==this) neighbours.remove(this);
      }
    }

    numberOfNeighbours = 0;
    // GRAVITATE AWAY FROM NEIGHBOURS:
    // MOVE WITH NEIGHBOURS:

    if (neighbours.size()>0) {
      for (int i=0;i<neighbours.size();i++) {
        Drop drop = (Drop)neighbours.elementAt(i);
        float dx = drop.getX() - getX();
        float dy = drop.getY() - getY();
        float d2 = dx*dx+dy*dy;
        float d = sqrt(d2);
        if (d<INFLUENCE_DISTANCE&&d!=0) {
          numberOfNeighbours++;
          float F =  INFLUENCE_STRENGTH/d2;
          float accX = F*dx/d;
          float accY = F*dy/d;
          float accStren = sqrt(accX*accX+accY*accY);
          if (accStren>INFLUENCE_MAX_STRENGTH) {
            accX = accX * INFLUENCE_MAX_STRENGTH / accStren;
            accY = accY * INFLUENCE_MAX_STRENGTH / accStren;
          }
          drop.accX(accX);
          drop.accY(accY);

          float p = INFLUENCE_MOVEMENT * d/INFLUENCE_DISTANCE;
          xm += p*drop.getMovX();
          ym += p*drop.getMovY();
        }
      }
    }

    if (gravity) {
      ym += GRAVITY_STRENGTH;
    }


    float flow = (numberOfNeighbours-FRICTION_NEIGHBOUR_AMOUNT_MIN) / (FRICTION_NEIGHBOUR_AMOUNT_MAX-FRICTION_NEIGHBOUR_AMOUNT_MIN);
    flow *= FRICTION_NEIGHBOUR_DECREASE_MAX;
    if (flow<0) flow=0;

    float fricSat = drawingSurface.getPaperSaturation(x,y);
    fricSat -= FRICTION_SATURATION_MIN;
    fricSat *= FRICTION_REDUCTION_FROM_SATURATION_MAX;
    if (fricSat<0) fricSat=0;

    float friction = 1.0 - (FRICTION - flow - fricSat);

    xm *= friction;
    ym *= friction;
  }




  public void drawOn(DrawingSurface drawingSurface) {

    //float xx = x;
    //float yy = y;

    for (int i=0;i<20;i++) {
      float xx = x + random(-DRAW_RADIUS,DRAW_RADIUS);
      float yy = y + random(-DRAW_RADIUS,DRAW_RADIUS);
      float sat = drawingSurface.getPaperSaturation(xx,yy);

      float emptyness = 1.0 - sat;
      emptyness *= 0.9; // never completely empty
      fluidAmount -= DROP_FLUID_LOSS_MAX*emptyness;    
      float pressure = DROP_DRAWING_STRENGTH_MAX * emptyness;

      softSetOn(xx,yy,DRAW_COLOR,pressure,drawingSurface);
      drawingSurface.addPaperSaturation(xx,yy,pressure);

      if (fluidAmount<=0) drawingSurface.removeDrop(this);
    }

  }


  public void toScreen() {
    float r = fluidAmount * DRAW_RADIUS;
    line(x-r,y,x+r,y);
    line(x,y-r,x,y+r);
  }


}
