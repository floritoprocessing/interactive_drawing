class ParticleGun {
  
  
  float x=0, y=0;
  float stren=1.0;
  float orientation=0.0;
  float orientationDeviation=0.0;
  
  
  public static final int TURN_LEFT = -1;
  public static final int TURN_RIGHT = 1;
  int autoTurnDirection = TURN_LEFT;
  
  
  ParticleGun() {
  }
  
  
  void listenToInput() {
    if (keyPressed) {
      if (key==CODED) {
        if (keyCode==RIGHT) {
          turn(TURN_RIGHT);
        } else if (keyCode==LEFT) {
          turn(TURN_LEFT);
        }
      }
    }
  }
  
  void setPosition(float _x, float _y) {
    x=_x;
    y=_y;
  }
  
  
  void setStrength(float _stren) {
    stren=_stren;
  }
  
  
  float getOrientation() {
    return orientation;
  }
  
  void setOrientation(float rd) {
    orientation = rd;
  }
  
  
  void setOrientationDeviation(float orDev) {
    orientationDeviation = orDev;
  }
  
  
  void autoTurn() {
    if (autoTurnDirection==TURN_LEFT) {
      orientation-=PI/10000.0;
    } else if (autoTurnDirection==TURN_RIGHT) {
      orientation+=PI/10000.0;
    }
  }
  
  void turn(int _t) {
    if (_t==TURN_LEFT) {
      orientation-=PI/1000.0;
    } else if (_t==TURN_RIGHT) {
      orientation+=PI/1000.0;
    }
  }
  
  
  void fire(ParticleCollection pc) {
    float oDev = orientationDeviation*random(-0.5,0.5);
    float xm = stren * cos(orientation+oDev);
    float ym = stren * sin(orientation+oDev);
    float ranStartDist = random(1.0);
    pc.newParticle(x+ranStartDist*xm,y+ranStartDist*ym,xm,ym);
  }
  
  
  void draw() {
    pushMatrix();
    ellipseMode(CENTER_RADIUS);
    stroke(64,64,64);
    fill(128,128,128);
    ellipse(x,y,5,5);
    rectMode(CORNER);
    translate(x,y);
    pushMatrix();
    rotateZ(orientation);
    rect(5,-2.5,20,5);
    popMatrix();
    popMatrix();
  }
}
