<div id="ChargedParticles_container">

<!--[if !IE]> -->
	<object classid="java:ChargedParticles.class" 
	type="application/x-java-applet"
	archive="ChargedParticles.jar"
	width="640" height="480"
	standby="Loading Processing software..." >
	
		<param name="archive" value="ChargedParticles.jar" />
		<param name="codebase" value="./applets/ChargedParticles/applet" />
		<param name="mayscript" value="true" />
		<param name="scriptable" value="true" />
	
		<param name="image" value="loading.gif" />
		<param name="boxmessage" value="Loading Processing software..." />
		<param name="boxbgcolor" value="#FFFFFF" />
	
		<param name="test_string" value="outer" />
<!--<![endif]-->
	
	<object classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" 
			codebase="http://java.sun.com/update/1.5.0/jinstall-1_5_0_15-windows-i586.cab"
			width="640" height="480"
			standby="Loading Processing software..."  >
			
		<param name="codebase" value="./applets/ChargedParticles/applet" />
		<param name="code" value="ChargedParticles" />
		<param name="archive" value="ChargedParticles.jar" />
		
		<param name="mayscript" value="true" />
		<param name="scriptable" value="true" />
		
		<param name="image" value="loading.gif" />
		<param name="boxmessage" value="Loading Processing software..." />
		<param name="boxbgcolor" value="#FFFFFF" />
		
		<param name="test_string" value="inner" />
		
		<p>
			<strong>
				This browser does not have a Java Plug-in.
				<br />
				<a href="http://java.sun.com/products/plugin/downloads/index.html" title="Download Java Plug-in">
					Get the latest Java Plug-in here.
				</a>
			</strong>
		</p>
	
	</object>
	
<!--[if !IE]> -->
	</object>
<!--<![endif]-->

</div>

<p>
<pre>
Charged Particles

- Press [1] to go to Experiment Mode
- Press [2] to go to Draw Mode

EXPERIMENT MODE:
- Press [+] / [-] to create a new charge at Mouse position. 
   (this charge will have the strength of the previously created charge)

- When hovering over a charge you can:
--- Click and drag to change its position
--- Press [D] to delete this charge
--- Press [UP] / [DOWN] to increase/decrease charge strength when hovering over a charge
- Press [LEFT] / [RIGHT] to change aim of Particle Gun(s)
RAW MODE:
The applet will draw a picture by itself by sweeping the particle gun(s);
</pre>
</p>

<p>
Source code: <a href="ChargedParticles.pde">ChargedParticles</a> <a href="Charge.pde">Charge</a> <a href="ChargeCollection.pde">ChargeCollection</a> <a href="GameMode.pde">GameMode</a> <a href="Particle.pde">Particle</a> <a href="ParticleCollection.pde">ParticleCollection</a> <a href="ParticleGun.pde">ParticleGun</a> 
</p>

<p>
Built with <a href="http://processing.org" title="Processing.org">Processing</a>
</p>