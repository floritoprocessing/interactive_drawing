/*
*
* Charged Particles
*
* - Press [1] to go to Experiment Mode
* - Press [2] to go to Draw Mode
*
* EXPERIMENT MODE:
* 
* - Press [+] / [-] to create a new charge at Mouse position. 
*   (this charge will have the strength of the previously created charge)
*
* - When hovering over a charge you can:
* --- Click and drag to change its position
* --- Press [D] to delete this charge
* --- Press [UP] / [DOWN] to increase/decrease charge strength when hovering over a charge
*
* - Press [LEFT] / [RIGHT] to change aim of Particle Gun(s)
*
* DRAW MODE:
*
* The applet will draw a picture by itself by sweeping the particle gun(s);
*
*/


String SAVE_PATH = "C:\\Documents and Settings\\mgraf\\My Documents\\";


public static final int MODE_EXPERIMENT = 0;
public static final int MODE_DRAW = 1;
int mainMode = MODE_EXPERIMENT;


ParticleGun gun = new ParticleGun();
ParticleCollection particles = new ParticleCollection();
ChargeCollection charges = new ChargeCollection();
boolean keyWasPressed=false;


void setup() {
  size(640,480,P3D);
  background(0,0,0);
  
  gun.setPosition(20,460);
  gun.setStrength(3.0);
  gun.setOrientation(-PI/8.0);
  gun.setOrientationDeviation(PI/5000.0);
  
  particles.setBounds(0,0,640,480);
  
}


void keyPressed() {
  keyWasPressed=true;
  
  if (mainMode==MODE_EXPERIMENT && key=='2') {
    mainMode = MODE_DRAW;
    background(0,0,0);
    particles.clear();
    particles.setDrawMode(Particle.DRAW_MODE_SOFT);
    println("Switching to drawing mode");
    gun.setOrientation(-PI/2.0-PI/8.0);
    gun.autoTurnDirection=ParticleGun.TURN_RIGHT;
  } 
  
  if (mainMode==MODE_DRAW && key=='1') {
    mainMode = MODE_EXPERIMENT;
    gun.setOrientation(-PI/4.0);
    particles.setDrawMode(Particle.DRAW_MODE_NORMAL);
    println("Switching to experiment mode");mainMode=MODE_EXPERIMENT;
  } else if (key=='S') {
    String name="ChargedParticles_"+nf(year(),4)+nf(month(),2)+nf(day(),2)+nf(hour(),2)+nf(minute(),2)+nf(second(),2)+".tga";
    save(SAVE_PATH+name);
    println(name+" saved in "+SAVE_PATH);
  }
  
}


void draw() {
  
  if (mainMode==MODE_EXPERIMENT) {
    
    
    
    // MODE EXPERIMENT:
    
    // listen to input:
    if (keyWasPressed) {
      gun.listenToInput();
      charges.listenToInput();
    }
    
    // fire gun and change charges and particles
    for (int i=0;i<50;i++) {      
      if (random(1.0)<0.7) gun.fire(particles);
      charges.update();
      particles.update(charges);
    }
    
    // draw charges, particles, gun
    background(0,0,0);
    charges.draw();
    particles.draw();
    gun.draw();
    
    
    
    
  } else if (mainMode==MODE_DRAW) {
    
    
    // MODE DRAW:
    
    for (int i=0;i<30;i++) {
      if (gun.getOrientation()<PI/8.0) {
        gun.autoTurn();
        gun.fire(particles);
        charges.update();
        particles.update(charges);
        particles.draw();
      }
    }
    
    
    
  }
  
  keyWasPressed=false;
}

