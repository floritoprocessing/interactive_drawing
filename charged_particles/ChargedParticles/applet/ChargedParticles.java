import processing.core.*; 
import processing.xml.*; 

import java.applet.*; 
import java.awt.*; 
import java.awt.image.*; 
import java.awt.event.*; 
import java.io.*; 
import java.net.*; 
import java.text.*; 
import java.util.*; 
import java.util.zip.*; 
import java.util.regex.*; 

public class ChargedParticles extends PApplet {

/*
*
* Charged Particles
*
* - Press [1] to go to Experiment Mode
* - Press [2] to go to Draw Mode
*
* EXPERIMENT MODE:
* 
* - Press [+] / [-] to create a new charge at Mouse position. 
*   (this charge will have the strength of the previously created charge)
*
* - When hovering over a charge you can:
* --- Click and drag to change its position
* --- Press [D] to delete this charge
* --- Press [UP] / [DOWN] to increase/decrease charge strength when hovering over a charge
*
* - Press [LEFT] / [RIGHT] to change aim of Particle Gun(s)
*
* DRAW MODE:
*
* The applet will draw a picture by itself by sweeping the particle gun(s);
*
*/


String SAVE_PATH = "C:\\Documents and Settings\\mgraf\\My Documents\\";


public static final int MODE_EXPERIMENT = 0;
public static final int MODE_DRAW = 1;
int mainMode = MODE_EXPERIMENT;


ParticleGun gun = new ParticleGun();
ParticleCollection particles = new ParticleCollection();
ChargeCollection charges = new ChargeCollection();
boolean keyWasPressed=false;


public void setup() {
  size(640,480,P3D);
  background(0,0,0);
  
  gun.setPosition(20,460);
  gun.setStrength(3.0f);
  gun.setOrientation(-PI/8.0f);
  gun.setOrientationDeviation(PI/5000.0f);
  
  particles.setBounds(0,0,640,480);
  
}


public void keyPressed() {
  keyWasPressed=true;
  
  if (mainMode==MODE_EXPERIMENT && key=='2') {
    mainMode = MODE_DRAW;
    background(0,0,0);
    particles.clear();
    particles.setDrawMode(Particle.DRAW_MODE_SOFT);
    println("Switching to drawing mode");
    gun.setOrientation(-PI/2.0f-PI/8.0f);
    gun.autoTurnDirection=ParticleGun.TURN_RIGHT;
  } 
  
  if (mainMode==MODE_DRAW && key=='1') {
    mainMode = MODE_EXPERIMENT;
    gun.setOrientation(-PI/4.0f);
    particles.setDrawMode(Particle.DRAW_MODE_NORMAL);
    println("Switching to experiment mode");mainMode=MODE_EXPERIMENT;
  } else if (key=='S') {
    String name="ChargedParticles_"+nf(year(),4)+nf(month(),2)+nf(day(),2)+nf(hour(),2)+nf(minute(),2)+nf(second(),2)+".tga";
    save(SAVE_PATH+name);
    println(name+" saved in "+SAVE_PATH);
  }
  
}


public void draw() {
  
  if (mainMode==MODE_EXPERIMENT) {
    
    
    
    // MODE EXPERIMENT:
    
    // listen to input:
    if (keyWasPressed) {
      gun.listenToInput();
      charges.listenToInput();
    }
    
    // fire gun and change charges and particles
    for (int i=0;i<50;i++) {      
      if (random(1.0f)<0.7f) gun.fire(particles);
      charges.update();
      particles.update(charges);
    }
    
    // draw charges, particles, gun
    background(0,0,0);
    charges.draw();
    particles.draw();
    gun.draw();
    
    
    
    
  } else if (mainMode==MODE_DRAW) {
    
    
    // MODE DRAW:
    
    for (int i=0;i<30;i++) {
      if (gun.getOrientation()<PI/8.0f) {
        gun.autoTurn();
        gun.fire(particles);
        charges.update();
        particles.update(charges);
        particles.draw();
      }
    }
    
    
    
  }
  
  keyWasPressed=false;
}

class Charge {
  
 
  float x=0, y=0;
  float mx=0, my=0;
  float load=0;
  float strength=1.0f;
  float SIZE=5;
  boolean selected = false;
  boolean lastFrameMouseDown = false;
  boolean dragging = true;
 
  
  public static final int CHARGE_POSITIVE=1;
  public static final int CHARGE_NEGATIVE=-1;
  
  
  Charge(float _x, float _y, float _load, float _strength) {
    x=_x;
    y=_y;
    load = _load;
    strength = _strength;
  }
  
  
  public void update() {
    // check if mouse is in the vecinity
    float dx=x-mouseX;
    float dy=y-mouseY;
    float d2=dx*dx+dy*dy;
    if (d2<SIZE*SIZE) selected=true; else selected=false;
    
    // check if dragging
    if (selected&&mousePressed&&!lastFrameMouseDown) {
      dragging=true;
      mx=x-mouseX;
      my=y-mouseY;
    }
    if (!mousePressed) dragging=false;
    lastFrameMouseDown=mousePressed;
    
    // drag!
    if (dragging) {
      x=mouseX+mx;
      y=mouseY+my;
    }
  }
  
  
  public void increaseStrength() {
    strength+=0.1f;
    if (strength>10.0f) strength=10.0f;
  }
  
  public void decreaseStrength() {
    strength-=0.1f;
    if (strength<0.1f) strength=0.1f;
  }
  
  
  public void draw() {
    ellipseMode(CENTER_RADIUS);
    noStroke();
    float b=80+175*strength/10.0f; // 0..10 -> 80..255
    if (load==CHARGE_POSITIVE) {
      fill(64,b,64);
    } else if (load==CHARGE_NEGATIVE) {
      fill(b,64,64);
    }
    ellipse(x,y,SIZE,SIZE);
    
    if (selected) {
      rectMode(CENTER);
      stroke(255,255,255);
      noFill();
      rect(x,y,2*(SIZE+2),2*(SIZE+2));
    }
  }
  
  
}
class ChargeCollection {
  
  
  Vector charges = new Vector();
  
  int nextCommand = 0;
  
  public static final int COMMAND_NONE = 0;
  public static final int COMMAND_DELETE_CHARGE = 1;
  public static final int COMMAND_CHARGE_STRENGTH_INCREASE = 2;
  public static final int COMMAND_CHARGE_STRENGTH_DECREASE = 3;
  
  
  ChargeCollection() {
  }
  
  
  public void setNextCommand(int c) {
    nextCommand = c;
  }
  
  
  
  public void listenToInput() {
    if (keyPressed) {
      if (key==CODED) {
        if (keyCode==UP) {
          setNextCommand(COMMAND_CHARGE_STRENGTH_INCREASE);
        } else if (keyCode==DOWN) {
          setNextCommand(COMMAND_CHARGE_STRENGTH_DECREASE);
        }
      } else {
        if (key=='+') {
          addCharge(Charge.CHARGE_POSITIVE);
        } else if (key=='-') {
          addCharge(Charge.CHARGE_NEGATIVE);
        } else if (key=='d'||key=='D') {
          setNextCommand(COMMAND_DELETE_CHARGE);
        }
      }
    }
    
  }
  
  
  public int chargeAmount() {
    return charges.size();
  }
  
  
  public Charge chargeAt(int i) {
    return ((Charge)(charges.elementAt(i)));
  }
  
  
  public void addCharge(int load) {
    float strength=0.5f;
    if (chargeAmount()>0) strength = ((Charge)(charges.elementAt(chargeAmount()-1))).strength;
    charges.add(new Charge(mouseX,mouseY,load,strength));
  }
  
  
  public void update() {
    for (int i=0;i<charges.size();i++) {
      Charge c = (Charge)(charges.elementAt(i));
      c.update();
      if (c.selected) {
        if (nextCommand==COMMAND_DELETE_CHARGE) {
          charges.remove(c);
        } else if (nextCommand==COMMAND_CHARGE_STRENGTH_INCREASE) {
          c.increaseStrength();
        } else if (nextCommand==COMMAND_CHARGE_STRENGTH_DECREASE) {
          c.decreaseStrength();
        }
      }
    }
    nextCommand=COMMAND_NONE;
  }
  
  
  public void draw() {
    for (int i=0;i<charges.size();i++) {
      ((Charge)(charges.elementAt(i))).draw();
    }
  }
  
}
class GameMode {
  
  
  GameMode() {
  }
  
  
}
class Particle {
  
  float x=0, y=0;
  float xm=0, ym=0;

  int drawMode = 255;
  float age=0;
  
  public static final int DRAW_MODE_NORMAL = 255;
  public static final int DRAW_MODE_SOFT = 32;
  
  Particle() {
  }
  
  
  Particle(float _x, float _y, float _xm, float _ym, int _dm) {
    x=_x;
    y=_y;
    xm=_xm;
    ym=_ym;
    drawMode = _dm;
  }
  
  
  public void setDrawMode(int dm) {
    drawMode=dm;
  }
  
  
  
  public void update(ChargeCollection cc) {
    if (drawMode==DRAW_MODE_SOFT) {
      if (age<=0.99f) age+=random(0.006f,0.01f);
    }
    
    float xa=0;
    float ya=0;
    float G=100.0f;
    
    for (int i=0;i<cc.chargeAmount();i++) {
      Charge c = cc.chargeAt(i);
      float dx = c.x - x;
      float dy = c.y - y;
      float r2 = dx*dx + dy*dy;
      float r = sqrt(r2);
      float a =  - G * c.load * c.strength / r2;
      if (r>c.SIZE) {
        float aDivR = a/r;
        xa += aDivR*dx;
        ya += aDivR*dy;
      }
    }
    
    xm+=xa;
    ym+=ya;
    
    x+=xm;
    y+=ym;
  }
 
  
  public void draw() {
    if (drawMode == DRAW_MODE_NORMAL) {
      //stroke(64,64,255);
      set((int)x,(int)y,color(64,64,255));
    } else if (drawMode == DRAW_MODE_SOFT) {
      int c=get((int)x,(int)y);
      float r=red(c);
      float g=green(c);
      float b=blue(c);
      r+=age*64/16.0f;
      g+=age*64/16.0f;
      b+=age*255/16.0f;
      if (r>255) r=255;
      if (g>255) g=255;
      if (b>255) b=255;
      set((int)x,(int)y,color(r,g,b));
    }    
  }
}
class ParticleCollection {
  
  
  Vector particles = new Vector();
  float left=0, right=1, bottom=1, top=0;
  int drawMode = Particle.DRAW_MODE_NORMAL;
  
  
  ParticleCollection() {
  }
  
  
  public void clear() {
    particles.clear();
  }
  
  
  public void setDrawMode(int dm) {
    drawMode = dm;
    for (int i=0;i<particles.size();i++) {
      Particle p = (Particle)(particles.elementAt(i));
      p.setDrawMode(drawMode);
    }
  }
  
  public void setBounds(float _left, float _top, float _right, float _bottom) {
    left = _left;
    right = _right;
    bottom = _bottom;
    top = _top;
  }
  
  
  public void newParticle(float _x, float _y, float _xm, float _ym) {
    particles.add(new Particle(_x,_y,_xm,_ym,drawMode));
  }
  
  
  public void update(ChargeCollection cc) {
    for (int i=0;i<particles.size();i++) {
      Particle p = (Particle)(particles.elementAt(i));
      p.update(cc);
      if (p.x<left||p.x>right||p.y<top||p.y>bottom) particles.remove(p);
    }
  }
  
  
  public void draw() {
    for (int i=0;i<particles.size();i++) {
      ((Particle)(particles.elementAt(i))).draw();
    }
  }
  
  
}
class ParticleGun {
  
  
  float x=0, y=0;
  float stren=1.0f;
  float orientation=0.0f;
  float orientationDeviation=0.0f;
  
  
  public static final int TURN_LEFT = -1;
  public static final int TURN_RIGHT = 1;
  int autoTurnDirection = TURN_LEFT;
  
  
  ParticleGun() {
  }
  
  
  public void listenToInput() {
    if (keyPressed) {
      if (key==CODED) {
        if (keyCode==RIGHT) {
          turn(TURN_RIGHT);
        } else if (keyCode==LEFT) {
          turn(TURN_LEFT);
        }
      }
    }
  }
  
  public void setPosition(float _x, float _y) {
    x=_x;
    y=_y;
  }
  
  
  public void setStrength(float _stren) {
    stren=_stren;
  }
  
  
  public float getOrientation() {
    return orientation;
  }
  
  public void setOrientation(float rd) {
    orientation = rd;
  }
  
  
  public void setOrientationDeviation(float orDev) {
    orientationDeviation = orDev;
  }
  
  
  public void autoTurn() {
    if (autoTurnDirection==TURN_LEFT) {
      orientation-=PI/10000.0f;
    } else if (autoTurnDirection==TURN_RIGHT) {
      orientation+=PI/10000.0f;
    }
  }
  
  public void turn(int _t) {
    if (_t==TURN_LEFT) {
      orientation-=PI/1000.0f;
    } else if (_t==TURN_RIGHT) {
      orientation+=PI/1000.0f;
    }
  }
  
  
  public void fire(ParticleCollection pc) {
    float oDev = orientationDeviation*random(-0.5f,0.5f);
    float xm = stren * cos(orientation+oDev);
    float ym = stren * sin(orientation+oDev);
    float ranStartDist = random(1.0f);
    pc.newParticle(x+ranStartDist*xm,y+ranStartDist*ym,xm,ym);
  }
  
  
  public void draw() {
    pushMatrix();
    ellipseMode(CENTER_RADIUS);
    stroke(64,64,64);
    fill(128,128,128);
    ellipse(x,y,5,5);
    rectMode(CORNER);
    translate(x,y);
    pushMatrix();
    rotateZ(orientation);
    rect(5,-2.5f,20,5);
    popMatrix();
    popMatrix();
  }
}

  static public void main(String args[]) {
    PApplet.main(new String[] { "--bgcolor=#ece9d8", "ChargedParticles" });
  }
}
