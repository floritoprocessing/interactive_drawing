class ParticleCollection {
  
  
  Vector particles = new Vector();
  float left=0, right=1, bottom=1, top=0;
  int drawMode = Particle.DRAW_MODE_NORMAL;
  
  
  ParticleCollection() {
  }
  
  
  void clear() {
    particles.clear();
  }
  
  
  void setDrawMode(int dm) {
    drawMode = dm;
    for (int i=0;i<particles.size();i++) {
      Particle p = (Particle)(particles.elementAt(i));
      p.setDrawMode(drawMode);
    }
  }
  
  void setBounds(float _left, float _top, float _right, float _bottom) {
    left = _left;
    right = _right;
    bottom = _bottom;
    top = _top;
  }
  
  
  void newParticle(float _x, float _y, float _xm, float _ym) {
    particles.add(new Particle(_x,_y,_xm,_ym,drawMode));
  }
  
  
  void update(ChargeCollection cc) {
    for (int i=0;i<particles.size();i++) {
      Particle p = (Particle)(particles.elementAt(i));
      p.update(cc);
      if (p.x<left||p.x>right||p.y<top||p.y>bottom) particles.remove(p);
    }
  }
  
  
  void draw() {
    for (int i=0;i<particles.size();i++) {
      ((Particle)(particles.elementAt(i))).draw();
    }
  }
  
  
}
