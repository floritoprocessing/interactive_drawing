class ChargeCollection {
  
  
  Vector charges = new Vector();
  
  int nextCommand = 0;
  
  public static final int COMMAND_NONE = 0;
  public static final int COMMAND_DELETE_CHARGE = 1;
  public static final int COMMAND_CHARGE_STRENGTH_INCREASE = 2;
  public static final int COMMAND_CHARGE_STRENGTH_DECREASE = 3;
  
  
  ChargeCollection() {
  }
  
  
  void setNextCommand(int c) {
    nextCommand = c;
  }
  
  
  
  void listenToInput() {
    if (keyPressed) {
      if (key==CODED) {
        if (keyCode==UP) {
          setNextCommand(COMMAND_CHARGE_STRENGTH_INCREASE);
        } else if (keyCode==DOWN) {
          setNextCommand(COMMAND_CHARGE_STRENGTH_DECREASE);
        }
      } else {
        if (key=='+') {
          addCharge(Charge.CHARGE_POSITIVE);
        } else if (key=='-') {
          addCharge(Charge.CHARGE_NEGATIVE);
        } else if (key=='d'||key=='D') {
          setNextCommand(COMMAND_DELETE_CHARGE);
        }
      }
    }
    
  }
  
  
  int chargeAmount() {
    return charges.size();
  }
  
  
  Charge chargeAt(int i) {
    return ((Charge)(charges.elementAt(i)));
  }
  
  
  void addCharge(int load) {
    float strength=0.5;
    if (chargeAmount()>0) strength = ((Charge)(charges.elementAt(chargeAmount()-1))).strength;
    charges.add(new Charge(mouseX,mouseY,load,strength));
  }
  
  
  void update() {
    for (int i=0;i<charges.size();i++) {
      Charge c = (Charge)(charges.elementAt(i));
      c.update();
      if (c.selected) {
        if (nextCommand==COMMAND_DELETE_CHARGE) {
          charges.remove(c);
        } else if (nextCommand==COMMAND_CHARGE_STRENGTH_INCREASE) {
          c.increaseStrength();
        } else if (nextCommand==COMMAND_CHARGE_STRENGTH_DECREASE) {
          c.decreaseStrength();
        }
      }
    }
    nextCommand=COMMAND_NONE;
  }
  
  
  void draw() {
    for (int i=0;i<charges.size();i++) {
      ((Charge)(charges.elementAt(i))).draw();
    }
  }
  
}
