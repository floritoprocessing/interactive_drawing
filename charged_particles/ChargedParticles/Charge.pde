class Charge {
  
 
  float x=0, y=0;
  float mx=0, my=0;
  float load=0;
  float strength=1.0;
  float SIZE=5;
  boolean selected = false;
  boolean lastFrameMouseDown = false;
  boolean dragging = true;
 
  
  public static final int CHARGE_POSITIVE=1;
  public static final int CHARGE_NEGATIVE=-1;
  
  
  Charge(float _x, float _y, float _load, float _strength) {
    x=_x;
    y=_y;
    load = _load;
    strength = _strength;
  }
  
  
  void update() {
    // check if mouse is in the vecinity
    float dx=x-mouseX;
    float dy=y-mouseY;
    float d2=dx*dx+dy*dy;
    if (d2<SIZE*SIZE) selected=true; else selected=false;
    
    // check if dragging
    if (selected&&mousePressed&&!lastFrameMouseDown) {
      dragging=true;
      mx=x-mouseX;
      my=y-mouseY;
    }
    if (!mousePressed) dragging=false;
    lastFrameMouseDown=mousePressed;
    
    // drag!
    if (dragging) {
      x=mouseX+mx;
      y=mouseY+my;
    }
  }
  
  
  void increaseStrength() {
    strength+=0.1;
    if (strength>10.0) strength=10.0;
  }
  
  void decreaseStrength() {
    strength-=0.1;
    if (strength<0.1) strength=0.1;
  }
  
  
  void draw() {
    ellipseMode(RADIUS);
    noStroke();
    float b=80+175*strength/10.0; // 0..10 -> 80..255
    if (load==CHARGE_POSITIVE) {
      fill(64,b,64);
    } else if (load==CHARGE_NEGATIVE) {
      fill(b,64,64);
    }
    ellipse(x,y,SIZE,SIZE);
    
    if (selected) {
      rectMode(CENTER);
      stroke(255,255,255);
      noFill();
      rect(x,y,2*(SIZE+2),2*(SIZE+2));
    }
  }
  
  
}
