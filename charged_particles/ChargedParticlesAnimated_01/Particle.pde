class Particle {
  
  float x=0, y=0;
  float xm=0, ym=0;
  float age=0;
  float PRESSURE = 1/16.0;
  
  Particle() {
  }
  
  
  Particle(float _x, float _y, float _xm, float _ym, float _press) {
    x=_x;
    y=_y;
    xm=_xm;
    ym=_ym;
    PRESSURE = _press;
  }
  
  
  void update(ChargeCollection cc) {
    if (age<=0.99) age+=random(0.006,0.01);
    
    float xa=0;
    float ya=0;
    float G=100.0;
    
    for (int i=0;i<cc.chargeAmount();i++) {
      Charge c = cc.chargeAt(i);
      float dx = c.x - x;
      float dy = c.y - y;
      float r2 = dx*dx + dy*dy;
      float r = sqrt(r2);
      float a =  - G * c.load * c.strength / r2;
      if (r>c.RANGE) {
        float aDivR = a/r;
        xa += aDivR*dx;
        ya += aDivR*dy;
      }
    }
    
    xm+=xa;
    ym+=ya;
    
    x+=xm;
    y+=ym;
  }
 
  
  void draw() {
    set((int)x,(int)y,color(64,64,255));
  }
  
  void drawOnCanvas(CustomCanvas cc) {
    cc.setPixel(x,y,64,64,255,age*PRESSURE);
    /*
    color c=get((int)x,(int)y);
    float r=red(c);
    float g=green(c);
    float b=blue(c);
    r+=age*64/16.0;
    g+=age*64/16.0;
    b+=age*255/16.0;
    if (r>255) r=255;
    if (g>255) g=255;
    if (b>255) b=255;
    set((int)x,(int)y,color(r,g,b));
    */
  }
  
  
}
