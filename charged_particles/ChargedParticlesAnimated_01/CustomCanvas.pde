class CustomCanvas {

  
  public static final int IMAGE_MODE_8BIT = 0;
  public static final int IMAGE_MODE_16BIT = 1;
  int imgMode = IMAGE_MODE_8BIT;
  
  
  public static final int BLEND_MODE_ADD = 0;
  int blendMode = BLEND_MODE_ADD;
  
  
  int width = 1;
  int height = 1;
  int[][][] chanImage;
  
  
  
  CustomCanvas(int _w, int _h, int _imgMode) {
    width = _w;
    height = _h;
    chanImage = new int[3][width][height];
    imgMode = _imgMode;
  }
  
  
  
  void switchImageModeFromTo(int modeFrom, int modeTo) {
    
    int option = 0;
    if (modeFrom==imgMode&&modeFrom==IMAGE_MODE_8BIT&&modeTo==IMAGE_MODE_16BIT) option = 1;
    if (modeFrom==imgMode&&modeFrom==IMAGE_MODE_16BIT&&modeTo==IMAGE_MODE_8BIT) option = 2;
    
    if (option==1) println("[F7] Switching from 8 to 16-bit");
    if (option==2) println("[F8] Switching from 16 to 8-bit");
    
    if (option!=0) {
      for (int ch=0;ch<3;ch++) {
        for (int x=0;x<width;x++) {
          for (int y=0;y<height;y++) {
            if (option==1) chanImage[ch][x][y] *= 256;
            if (option==2) chanImage[ch][x][y] /= 256;
          }
        }
      }
      imgMode = modeTo;
    }
  }
  
  
  
  void clear() {
    for (int ch=0;ch<3;ch++) {
      for (int x=0;x<width;x++) {
        for (int y=0;y<height;y++) {
          chanImage[ch][x][y] = 0;
        }
      }
    }
  }
  
  
  
  float mixChan(int a, int b) {

    float out = 0;
    
    // additive drawing:
    if (blendMode == BLEND_MODE_ADD) out = (int)(a+b);
    
    if (imgMode == IMAGE_MODE_8BIT) {
      if (out>255) out=255;
    } else if (imgMode == IMAGE_MODE_16BIT) {
      if (out>65535) out=65535;
    }    
    
    return out;
  }
  
  
  
  void setPixel(float _x, float _y, int rr, int gg, int bb, float p) {
    int x=(int)_x;
    int y=(int)_y;
    
    if (x>=0&&x<width&&y>=0&&y<height) {
      if (imgMode == IMAGE_MODE_16BIT) {
        rr*=256;
        gg*=256;
        bb*=256;
      }
      float pB = 1.0-p;
      chanImage[0][x][y] = (int)(pB*chanImage[0][x][y] + p*mixChan(rr,chanImage[0][x][y]));
      chanImage[1][x][y] = (int)(pB*chanImage[1][x][y] + p*mixChan(gg,chanImage[1][x][y]));
      chanImage[2][x][y] = (int)(pB*chanImage[2][x][y] + p*mixChan(bb,chanImage[2][x][y]));
    }
  }
  
  
  
  void draw() {
    int rr=0, gg=0, bb=0;
    if (imgMode == IMAGE_MODE_8BIT) {
      colorMode(RGB,255);
    } else if (imgMode == IMAGE_MODE_16BIT) {
      colorMode(RGB,65535);
    }   
    
    for (int x=0;x<width;x++) {
      for (int y=0;y<height;y++) {
        rr = chanImage[0][x][y];
        gg = chanImage[1][x][y];
        bb = chanImage[2][x][y];
        set(x,y,color(rr,gg,bb));
      }
    }
    colorMode(RGB,255);
  }
  
  
  
  void save(String filename) {
  }
  
  
}
