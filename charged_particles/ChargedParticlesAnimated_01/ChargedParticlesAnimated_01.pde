/*
*
* Charged Particles
*
* - Press [F1] to go to Experiment Mode
* - Press [F2] to go to Draw Mode
* - Press [F7]/[F8] to switch between 8-bit / 16-bit mode
* - Press [F9]..[F12] to change fineness for Draw Mode
*
* - Press [z] / [SHIFT]-[Z] to load/save preset 1 for charges
* - Press [x] / [SHIFT]-[X] to load/save preset 2 for charges
*
* EXPERIMENT MODE:
* 
* - Press [+] / [-] to create a new charge at Mouse position. 
*   (this charge will have the strength and range of the previously created charge)
*
* - When hovering over a charge you can:
* --- Click and drag to change its position
* --- Press [D] to delete this charge
* --- Press [Q] / [A] to increase/decrease minimum range
* --- Press [UP] / [DOWN] to increase/decrease charge strength when hovering over a charge
*
* - Press [LEFT] / [RIGHT] to change aim of Particle Gun(s)
*
* DRAW MODE:
*
* The applet will draw a picture by itself by sweeping the particle gun(s);
*
*/


String SAVE_PATH = "C:\\Documents and Settings\\mgraf\\My Documents\\";


public static final int MODE_EXPERIMENT = 0;
public static final int MODE_DRAW = 1;
int mainMode = MODE_EXPERIMENT;


CustomCanvas canvas;
ParticleGun gun = new ParticleGun();
ParticleCollection particles = new ParticleCollection();
ChargeCollection charges = new ChargeCollection();
boolean keyWasPressed=false;


void setup() {
  size(640,480,P3D);
  canvas = new CustomCanvas(640,480,CustomCanvas.IMAGE_MODE_8BIT);
  particles.setBounds(0,0,640,480);  
}


void keyPressed() {
  keyWasPressed=true;
  
  
  if (key==CODED&&keyCode==112) {
    
    
    mainMode = MODE_EXPERIMENT;
    gun.setOrientation(-PI/4.0);
    println("[F1] Switching to experiment mode");
     
      
  } else if (key==CODED&&keyCode==113) {
    
    
    mainMode = MODE_DRAW;
    background(0,0,0);
    canvas.clear();
    particles.clear();
    println("[F2] Switching to drawing mode");
    gun.setOrientation(-PI/2.0-PI/8.0);
    gun.autoTurnDirection=ParticleGun.TURN_RIGHT;
    
      
  } else if (key==CODED&&keyCode==118) {
    
    
    canvas.switchImageModeFromTo(CustomCanvas.IMAGE_MODE_16BIT,CustomCanvas.IMAGE_MODE_8BIT);
    
    
  } else if (key==CODED&&keyCode==119) {
    
    
    canvas.switchImageModeFromTo(CustomCanvas.IMAGE_MODE_8BIT,CustomCanvas.IMAGE_MODE_16BIT);
    
    
  } else if (key==CODED&&keyCode>=120&&keyCode<124) {
    
    float f = 1/pow(2,(keyCode-121)*2);
    println("[F"+(keyCode-111)+"] Drawing mode fineness: "+(keyCode-119)+" ("+f+")");
    gun.AUTO_TURN_STEP = f*PI/7500.0;
    gun.PARTICLE_PRESSURE = f*1/8.0;
    
    
  } else if (key=='z') {
    
    // [z] : load Preset 1
    print("["+key+"] ");
    charges.loadPreset(0);
    
  } else if (key=='Z') {
    
    // [Z] : save Preset 1
    print("["+key+"] ");
    charges.savePreset(0);
    
  } else if (key=='x') {
    
    // [x] : load Preset 2
    print("["+key+"] ");
    charges.loadPreset(1);
    
  } else if (key=='X') {
    
    // [X] : save Preset 2
    print("["+key+"] ");
    charges.savePreset(1);
    
  } else if (key=='S') {
    
    
    String name="ChargedParticles_"+nf(year(),4)+nf(month(),2)+nf(day(),2)+nf(hour(),2)+nf(minute(),2)+nf(second(),2)+".tga";
    save(SAVE_PATH+name);
    println("Saving screen to "+name+" in folder "+SAVE_PATH);
    
    
  }
  
  
  //if (key==CODED) {
   // println("Key Coded: "+keyCode);
    //if (key=='r') println("R");
  //}
}


void draw() {
  
  
  if (mainMode==MODE_EXPERIMENT) {
    
    
    // ***************
    // MODE EXPERIMENT
    // ***************
    
    
    // listen to input:
    if (keyWasPressed) {
      gun.listenToInput();
      charges.listenToInput();
    }
    
    // fire gun and change charges and particles
    for (int i=0;i<50;i++) {      
      if (random(1.0)<0.7) gun.fire(particles);
      charges.update();
      particles.update(charges);
    }
    
    // draw charges, particles, gun
    background(0,0,0);
    charges.draw();
    particles.draw();
    gun.draw();
    
    
  } else if (mainMode==MODE_DRAW) {
    
    
    // *********
    // MODE DRAW
    // *********
    
    
    for (int i=0;i<30;i++) {
      if (gun.getOrientation()<PI/8.0) {
        gun.autoTurn();
        gun.fire(particles);
        charges.update();
        particles.update(charges);
        particles.drawOnCanvas(canvas);
      }
    }
    canvas.draw();
    
    
    
  }
  
  
  
  keyWasPressed=false;
  
}

