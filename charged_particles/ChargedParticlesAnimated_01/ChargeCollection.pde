class ChargeCollection {
  
  
  int nextCommand = 0;
  
  public static final int COMMAND_NONE = 0;
  public static final int COMMAND_DELETE_CHARGE = 1;
  public static final int COMMAND_CHARGE_STRENGTH_INCREASE = 2;
  public static final int COMMAND_CHARGE_STRENGTH_DECREASE = 3;
  public static final int COMMAND_CHARGE_INCREASE_RANGE = 4;
  public static final int COMMAND_CHARGE_DECREASE_RANGE = 5;
  
  Vector charges = new Vector();
  
  Vector[] preset = new Vector[2];
  
  
  ChargeCollection() {
    preset[0] = new Vector();
    preset[1] = new Vector();
  }
  
  
  void savePreset(int nr) {
    if (charges.size()>0) {
      println(" saving preset "+(nr+1)+" - Charges: "+charges.size());
      preset[nr] = new Vector();
      for (int i=0;i<charges.size();i++) {
        Charge c = new Charge((Charge)(charges.elementAt(i)));
        preset[nr].add(c);
      }
    }
  }
  
  
  
  void loadPreset(int nr) {
    if (preset[nr].size()>0) {
      println(" loading preset "+(nr+1)+" - Preset charges: "+preset[nr].size());
      charges = new Vector();
      for (int i=0;i<preset[nr].size();i++) {
        Charge c = new Charge((Charge)(preset[nr].elementAt(i)));
        charges.add(c);
      }
    }
  }
  
  
  
  void setNextCommand(int c) {
    nextCommand = c;
  }
  
  
  
  void listenToInput() {
    if (keyPressed) {
      if (key==CODED) {
        if (keyCode==UP) {
          setNextCommand(COMMAND_CHARGE_STRENGTH_INCREASE);
        } else if (keyCode==DOWN) {
          setNextCommand(COMMAND_CHARGE_STRENGTH_DECREASE);
        }
      } else {
        if (key=='+') {
          addCharge(Charge.CHARGE_POSITIVE);
        } else if (key=='-') {
          addCharge(Charge.CHARGE_NEGATIVE);
        } else if (key=='d'||key=='D') {
          setNextCommand(COMMAND_DELETE_CHARGE);
        } else if (key=='q'||key=='Q') {
          setNextCommand(COMMAND_CHARGE_INCREASE_RANGE);
        } else if (key=='a'||key=='A') {
          setNextCommand(COMMAND_CHARGE_DECREASE_RANGE);
        }
      }
    }
    
  }
  
  
  int chargeAmount() {
    return charges.size();
  }
  
  
  Charge chargeAt(int i) {
    return ((Charge)(charges.elementAt(i)));
  }
  
  
  void addCharge(int load) {
    float strength=0.5;
    float range=5.0;
    if (chargeAmount()>0) {
      strength = ((Charge)(charges.elementAt(chargeAmount()-1))).strength;
      range = ((Charge)(charges.elementAt(chargeAmount()-1))).RANGE;
    }
    charges.add(new Charge(mouseX,mouseY,load,strength,range));
  }
  
  
  void update() {
    for (int i=0;i<charges.size();i++) {
      Charge c = (Charge)(charges.elementAt(i));
      c.update();
      if (c.selected) {
        if (nextCommand==COMMAND_DELETE_CHARGE) {
          charges.remove(c);
        } else if (nextCommand==COMMAND_CHARGE_STRENGTH_INCREASE) {
          c.increaseStrength();
        } else if (nextCommand==COMMAND_CHARGE_STRENGTH_DECREASE) {
          c.decreaseStrength();
        } else if (nextCommand==COMMAND_CHARGE_INCREASE_RANGE) {
          c.increaseRange();
        } else if (nextCommand==COMMAND_CHARGE_DECREASE_RANGE) {
          c.decreaseRange();
        }
      }
    }
    nextCommand=COMMAND_NONE;
  }
  
  
  void draw() {
    for (int i=0;i<charges.size();i++) {
      ((Charge)(charges.elementAt(i))).draw();
    }
  }
  
}
