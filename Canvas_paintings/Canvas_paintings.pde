void setup() {
  size(640,480,P3D);
  newCanvas();
  newBrush(47,12,247);
}

void draw() {
  background(230,240,255);
  translate(width/2,height/2); rotateX(45*PI/180.0);
  canvas.update();
  brush.update();
}

////////////////////////////// BRUSH VALUES AND CLASS //////////////////////////////

Brush brush;

void newBrush(int r, int g, int b) {
  brush=new Brush(r<<16|g<<8|b);
}

class Brush {
  int col;    // color
  float rd;   // direction
  Hair[] hair; int nrOfHairs=1;
  
  Brush(int col) {
    this.col=col;
    hair=new Hair[nrOfHairs]; 
    for (int i=0;i<nrOfHairs;i++) {
      hair[i]=new Hair(0,0,100,10);
    }
  }
  
  void update() {
    int x=mouseX-width/2, y=mouseY-height/2;
    pushMatrix(); translate(x,y,100); box(10); popMatrix();
    
    for (int i=0;i<nrOfHairs;i++) {
      hair[i].toScreen(x,y,100);
    }
  }
  
}

////////////////////////////// end BRUSH //////////////////////////////

////////////////////////////// HAIR //////////////////////////////

class Hair {
  subHair[] subhair;
  float x,y,len,subs;
  Hair(float x, float y, float len, int subs) {
    this.x=x; this.y=y; this.subs=subs;
    subhair=new subHair[subs];
    for (int i=0;i<subs;i++) {
      float sublen=len/(float)subs;
      subhair[i]=new subHair(x,y,len-i*sublen,sublen);
    }
  }
  void toScreen(float x, float y, float z) {
    pushMatrix();
    stroke(255,0,0);
    for (int i=0;i<subs;i++) {
      if (i==0) {
        subhair[i].update(x,y,z);
      } else {
        subhair[i].update(subhair[i-1].x2,subhair[i-1].y2,subhair[i-1].z2);
      }
      line(subhair[i].x1,subhair[i].y1,subhair[i].z1,subhair[i].x2,subhair[i].y2,subhair[i].z2);
    }
    popMatrix();
  }
}

class subHair {
  float x1,y1,z1;
  float x2,y2,z2;
  float len;
  subHair(float x, float y, float z, float len) {
    x1=x; y1=y; z1=z; 
    x2=x; y2=y; z2=z+len;
  }
  void update(float tx, float ty, float tz) {
    
  }
}

////////////////////////////// end HAIR //////////////////////////////

////////////////////////////// CANVAS VALUES AND CLASS //////////////////////////////

Canvas canvas;

void newCanvas() {
  canvas=new Canvas("empty_640x480.jpg");
  canvas.update();
}

class Canvas {
  PImage bmp;
  int w, h, maxpix; float w2,h2;
  Canvas(String bmpName) { 
    bmp=loadImage(bmpName);
    w=bmp.width; h=bmp.height; maxpix=w*h; w2=w/2.0; h2=h/2.0;
    for (int x=0;x<w;x++) { for (int y=0;y<h;y++) {
      int i=y*w+x;
      float frq=120, pha=random(0,TWO_PI);
      float lineX=0.5+sin(frq*TWO_PI*x/(float)w+pha)*0.5;
      float lineY=0.5+sin(frq*TWO_PI*y/(float)w+pha)*0.5;
      float li=sq(lineX*lineY);
      lineY=0.5+sin(frq*TWO_PI*-y/(float)w+pha)*0.5;
      li+=sq(lineX*lineY);
      li/=2.0;li=1-li;li=sqrt(li);
      int b=int(255*li);
      bmp.pixels[i]=b<<16|b<<8|b;
    } }
  }
  
  void update() {
    stroke(0);
    beginShape(QUADS); 
    texture(bmp); 
    vertex(-w2,-h2,0,0,0); 
    vertex(w2,-h2,0,w,0); 
    vertex(w2,h2,0,w,h); 
    vertex(-w2,h2,0,0,h); 
    endShape(); 
  }
}

////////////////////////////// end CANVAS //////////////////////////////
