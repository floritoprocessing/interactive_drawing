int i;
int nrOfDots=500;
Dot[] d=new Dot[nrOfDots+1];

void setup() {
  size(400,400,P3D);
  
  for (i=1;i<=nrOfDots;i++) {
    d[i]=new Dot(random(200),random(200),i);
  }
  
  background(255);
}

void draw() {
  background(255);
  for (i=1;i<=nrOfDots;i++) {
    d[i].update();
  }
}

class Dot {
  float tx,ty,xpos,ypos;
  int nr;
  Dot (float x,float y, int n) {
    nr=n;
    xpos=x;
    ypos=y;
  }
  
  void update() {
    if (nr==1) {
      tx=mouseX;
      ty=mouseY;
    } else {
      tx=d[nr-1].xpos;
      ty=d[nr-1].ypos;
    }
    xpos=(3*xpos+tx)/4;
    ypos=(3*ypos+ty)/4;
    point(xpos,ypos);
  }
}
