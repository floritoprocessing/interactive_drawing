class Pen {
  
  public static final int BRUSH_TYPE_PIXEL = 0;
  int brushType = BRUSH_TYPE_PIXEL;
  
  int col = 0xA00000;
  
  float opacity = 0.3;
  
  Pen() {
  }
  
  Pen(Pen p) {
    brushType = p.brushType;
    col = p.col;
    opacity = p.opacity;
  }
  
  void setBrush(int bt) {
    brushType=bt;
  }
  
  void drawAt(float x1, float y1, float x2, float y2) {
    
    float dx = x2-x1;
    float dy = y2-y1;
    float len = sqrt(dx*dx+dy*dy);
    
    for (float i=0;i<len;i++) {
      float perc = i/len;
      float x = x1 + perc * dx;
      float y = y1 + perc * dy;
      
      if (brushType==BRUSH_TYPE_PIXEL) {
        setSoft((int)x,(int)y,col,opacity);
      }
      
    }
    
    
  }
  
}
