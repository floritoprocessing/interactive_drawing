import java.util.Vector;

Pen pen = new Pen();

Drawing drawing;
DrawingsContainer drawingsContainer = new DrawingsContainer();

boolean lastMousePressed = false;

void setup() {
  size(800,400,P3D);
  background(255,255,255);
  pen.setBrush(Pen.BRUSH_TYPE_PIXEL);
}




void draw() {
  boolean mp = mousePressed;
  
  
  // MOUSE DOWN EVENT:
  if (mp&&!lastMousePressed) {
    drawing = new Drawing();
    drawing.setPen(pen);
  }
    
  if (mp) {
    pen.drawAt(pmouseX,pmouseY,mouseX,mouseY);
    drawing.addDot(mouseX,mouseY);
  }
  
  
  // MOUSE UP EVENT:
  if (!mp&&lastMousePressed) {
    drawing.finalize();
    drawingsContainer.add(drawing);
    //drawingsContainer.debug();
  }
  
  
  drawingsContainer.drawMorphed(1);
  
  
  lastMousePressed = mp;
}
