class DrawingsContainer {
  
  Vector drawings;
  
  DrawingsContainer() {
    drawings = new Vector();
  }
  
  void add(Drawing d) {
    drawings.add(d);
  }
  
  
  
  void drawMorphed(int amount) {
    
    int poolSize = drawings.size();
    if (poolSize==0) return;
    
    for (int i=0;i<amount;i++) {
      int r = (int)random(poolSize);
      Drawing d = (Drawing)(drawings.elementAt(r));
      Drawing m = d.createClone();
      m.scaleDots(random(0.8,1.2));
      m.draw();
    }
    
  }
  
  
  
  void debug() {
    println("nr of drawing: "+drawings.size());
    for (int i=0;i<drawings.size();i++) {
      Drawing d = (Drawing)(drawings.elementAt(i));
      println("Drawing nr "+i);
      d.showCoordinates();
    }
  }
  
}
