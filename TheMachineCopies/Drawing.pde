class Drawing {
  
  Pen pen;
  Vector dots;
  
  int nrOfDots = 0;
  float avX=0, avY=0;
  
  Drawing() {
    dots = new Vector();
  }
  
  void setPen(Pen p) {
    pen = new Pen(p);
  }
  
  void addDot(float x, float y) {
    float[] dot = new float[2];
    dot[0]=x;
    dot[1]=y;
    dots.add(dot);
  }
  
  Drawing createClone() {
    Drawing c = new Drawing();
    c.setPen(pen);
    c.dots = (Vector)dots.clone();
    c.avX = avX;
    c.avY = avY;
    return c;
  }
  
  void scaleDots(float p) {
    for (int i=0;i<dots.size();i++) {
      float[] dot = (float[])(dots.elementAt(i));
      dot[0]-=avX;
      dot[1]-=avY;
      dot[0]*=p;
      dot[1]*=p;
      dot[0]+=avX;
      dot[1]+=avY;
      dots.setElementAt(dot,i) ;
    }
  }
  
  void finalize() {
    for (int i=0;i<dots.size();i++) {
      float[] dot = (float[])(dots.elementAt(i));
      avX += dot[0];
      avY += dot[1];
    }
    avX /= ((float)dots.size());
    avY /= ((float)dots.size());
  }
  
  void draw() {
    for (int i=0;i<dots.size()-1;i++) {
      float[] dot0 = (float[])(dots.elementAt(i));
      float[] dot1 = (float[])(dots.elementAt(i+1));
      pen.drawAt(dot0[0],dot0[1],dot1[0],dot1[1]);
    }
  }
  
  void showCoordinates() {
    for (int i=0;i<dots.size();i++) {
      float[] dot = (float[])(dots.elementAt(i));
      println(dot[0]+"\t"+dot[1]);
    }
  }
  
}
