
void setSoft(int x, int y, color c1, float opacity) {
  color c2 = get(x,y);
  float opac2 = 1.0-opacity;
  
  int r1=c1>>16&0xFF;
  int g1=c1>>8&0xFF;
  int b1=c1&0xFF;
  
  int r2=c2>>16&0xFF;
  int g2=c2>>8&0xFF;
  int b2=c2&0xFF;
  
  int rOut=(int)(opacity*r1 + opac2*r2);
  int gOut=(int)(opacity*g1 + opac2*g2);
  int bOut=(int)(opacity*b1 + opac2*b2);
  
  set(x,y,rOut<<16|gOut<<8|bOut);
}
