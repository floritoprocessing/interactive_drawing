package pendrawing.brush;

import java.awt.Point;

public class BrushCreator extends BrushTools {

	public static Brush createCircular(float innerRadius, float outerRadius, float innerOpacity, float outerOpacity, int color) {
		
		int radius = (int)Math.ceil(outerRadius);
		int size = 2*radius+1;
		Point[] point = new Point[size*size];
		int[] rgb = new int[size*size];
		float[] opacity = new float[size*size];
		
		int i=0;
		for (int x=-radius;x<=radius;x++) {
			for (int y=-radius;y<=radius;y++) {
				float d = (float)Math.sqrt(x*x+y*y);
				point[i] = new Point(x,y);
				rgb[i] = color;
				if (d<=innerRadius) {
					opacity[i]=innerOpacity;
					//rgb[i] = innerColor;
				} else if (d<=outerRadius) {
					float inOutPercentage = (d-innerRadius)/(outerRadius-innerRadius);
					//rgb[i] = interpolateRGB(innerColor, outerColor, inOutPercentage);
					opacity[i] = innerOpacity + (outerOpacity-innerOpacity) * inOutPercentage;
				} else {
					opacity[i]=outerOpacity;
					//rgb[i] = outerColor;
				}
				i++;
			}
		}
		
		return new Brush(point,rgb,opacity);
	}

}
