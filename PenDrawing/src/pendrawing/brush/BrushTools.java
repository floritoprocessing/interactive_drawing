package pendrawing.brush;

public class BrushTools {

	/**
	 * Interpolate two colors.
	 * The colors will be clamped, meaning a percentage outside 0..1 will be ignored.<p>
	 * Alpha values are ignored (bit 24..31).
	 * @param color0
	 * @param color1
	 * @param percentage
	 * @return
	 */
	public static int interpolateRGB(int color0, int color1, float percentage) {
		return interpolateRGB(color0, color1, percentage, true);
	}
	
	/**
	 * Interpolate two colors.
	 * If clamp is false, percentages below 0 and higher than 1 will result in color are extrapolation.
	 * Color values are clamped at 0x000000..0xffffff<p>
	 * Alpha values are ignored (bit 24..31).
	 * @param color0
	 * @param color1
	 * @param percentage
	 * @param clamp
	 * @return
	 */
	public static int interpolateRGB(int color0, int color1, float percentage, boolean clamp) {
		int r0 = (color0>>16&0xff);
		int r1 = (color1>>16&0xff);
		int g0 = (color0>>8&0xff);
		int g1 = (color1>>8&0xff);
		int b0 = (color0&0xff);
		int b1 = (color1&0xff);
		int dr = r1-r0;
		int dg = g1-g0;
		int db = b1-b0;
		if (clamp) {
			percentage = Math.max(0,Math.min(1,percentage));
		}
		int r = (int)(r0+percentage*dr);
		int g = (int)(g0+percentage*dg);
		int b = (int)(b0+percentage*db);
		if (r<0) r=0;
		if (r>255) r=255;
		if (g<0) g=0;
		if (g>255) g=255;
		if (b<0) b=0;
		if (b>255) b=255;
		return r<<16|g<<8|b;
	}
	
	/**
	 * Mixes two colors. Clamps the output RGB to 0x000000..0xffffff.<p>
	 * Alpha values are ignored (bit 24..31).
	 * @param color0
	 * @param color1
	 * @param percentage0
	 * @param percentage1
	 * @return
	 */
	public static int mixRGB(int color0, int color1, float percentage0, float percentage1) {
		int r = (int)( percentage0*(color0>>16&0xff) + percentage1*(color1>>16&0xff));
		int g = (int)( percentage0*(color0>>8&0xff) + percentage1*(color1>>8&0xff));
		int b = (int)( percentage0*(color0&0xff) + percentage1*(color1&0xff));
		if (r<0) r=0;
		if (r>255) r=255;
		if (g<0) g=0;
		if (g>255) g=255;
		if (b<0) b=0;
		if (b>255) b=255;
		return r<<16|g<<8|b;
	}
	
}
