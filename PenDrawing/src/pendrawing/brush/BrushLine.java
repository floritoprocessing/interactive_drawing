package pendrawing.brush;

import java.awt.Point;

import pendrawing.brush.AbstractBrush.Method;
import processing.core.PGraphics;
import processing.core.PImage;


//TODO

/**
 * http://www.cs.unc.edu/~mcmillan/comp136/Lecture6/Lines.html
 * Drawing a line with a brush like this is not correct yet
 * This is not a brush, it's actually a Symbol (using Adobe terms)
 * 
 * Instead,
 * 
 * - Create image that can contain the line
 * - Draw the line just using premultiplied RGB
 * - Set all alpha to 0
 * - Draw the line just using alpha, keeping always maximum value of alpha
 * - Now draw the created image onto the target
 * (it's now possible to also use blending modes)  
 *
 * @author mgraf
 *
 */
public class BrushLine {
	
	private Point p0, p1;
		
	public BrushLine(Point p0, Point p1) {
		this.p0 = new Point(p0);
		this.p1 = new Point(p1);
	}
	
	public BrushLine(int x0, int y0, int x1, int y1) {
		this.p0 = new Point(x0,y0);
		this.p1 = new Point(x1,y1);
	}
	
	public void drawVersion2(PImage pg, AbstractBrush shape) {
		
		/*
		 * Create image that can contain the line
		 */
		int shapeTop = shape.getEdgeValues()[0];
		int shapeLef = shape.getEdgeValues()[1];
		int shapeBot = shape.getEdgeValues()[2];
		int shapeRig = shape.getEdgeValues()[3];
		
		int lineTop = Math.min(p0.y,p1.y);
		int lineLef = Math.min(p0.x,p1.x);
		int lineBot = Math.max(p0.y,p1.y);
		int lineRig = Math.max(p0.x,p1.x);
		
		int imageTop = lineTop+shapeTop;
		int imageLef = lineLef+shapeLef;
		int imageBot = lineBot+shapeBot;
		int imageRig = lineRig+shapeRig;
		int imageWidth = imageRig-imageLef;
		int imageHeight = imageBot-imageTop;
		
		PImage img = new PImage(imageWidth,imageHeight);
		img.format = PImage.ARGB;
		/*for (int i=0;i<img.pixels.length;i++) {
			img.pixels[i] = 0xff000000;
		}*/
		
		/*
		 * - Draw the line just using premultiplied RGB
		 */
		translate(-imageLef, -imageTop);
		draw(img, shape, Method.PREMULTIPLIED_RGB_TO_0X00, false);
		draw(img, shape, Method.JUST_ALPHA, false);
		//draw(img, shape, Method.JUST_ALPHA, false);
		
		/*
		 * - Set all alpha to 0
		 */
		//img.format = PImage.ARGB;
		
		
		/*
		 * - Now draw the created image onto the target
		 * (it's now possible to also use blending modes)
		 */
		
		//pg.beginDraw();
		pg.blend(img, 0, 0, imageWidth, imageHeight, imageLef, imageTop, imageWidth, imageHeight, PImage.BLEND);
		//pg.image(img,imageLef,imageTop);
		//pg.endDraw();
		
	}
	
	public void draw(PImage pg, AbstractBrush shape, Method method, boolean sqrt) {//boolean smooth, boolean thick) {
		int x0 = p0.x;
		int x1 = p1.x;
		int y0 = p0.y;
		int y1 = p1.y;
		int dx = x1-x0;
		int dy = y1-y0;
		boolean horizontal = true;
		if (Math.abs(dx)<Math.abs(dy)) {
			horizontal = false;
		}
		if (horizontal) {
			int x=x0;
			float t = y0;
			int xadd = dx<0?-1:1;
			float m = (float) dy / (float) dx;
			m*=xadd;
			while(x!=x1) {
				shape.drawAt(pg, x, t, method, sqrt);
				x+=xadd;
				t+=m;
			}
		}
		else {
			int y=y0;
			float t = x0;
			int yadd = dy<0?-1:1;
			float m = (float)dx / (float)dy;
			m*=yadd;
			while (y!=y1) {
				shape.drawAt(pg, t, y, method, sqrt);
				t+=m;
				y+=yadd;
			}
		}
	}	

	public void translate(int xo, int yo) {
		p0.x += xo;
		p1.x += xo;
		p0.y += yo;
		p1.y += yo;
	}
	

}
