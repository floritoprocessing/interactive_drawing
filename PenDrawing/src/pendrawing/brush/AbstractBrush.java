package pendrawing.brush;

import java.awt.Point;

import processing.core.PGraphics;
import processing.core.PImage;

public abstract class AbstractBrush extends BrushTools {

	public abstract int size();
	public abstract Point getPoint(int i);
	public abstract int getRGB(int i);
	public abstract float getOpacity(int i);
	
	private final int[] edgeValues;// = new int[4];
	
	public static enum Method {
		PREMULTIPLIED_RGB_TO_0XFF (true,0xff000000),
		PREMULTIPLIED_RGB_TO_0X00 (true,0x00000000),
		JUST_ALPHA (false,0x00000000);
		
		private int mask;
		private boolean premultiplied;
		Method(boolean premultiplied, int mask) {
			this.premultiplied = premultiplied;
			this.mask = mask;
		}
		public int getMask() {
			return mask;
		}
		public boolean isPremultiplied() {
			return premultiplied;
		}
	}
	
	protected AbstractBrush(int[] topLeftBottomRight) {
		if (topLeftBottomRight==null) {
			throw new NullPointerException("Please supply the edge values");
		}
		edgeValues = new int[4];
		System.arraycopy(topLeftBottomRight, 0, edgeValues, 0, 4);
	}
	
	protected AbstractBrush(int top, int left, int bottom, int right) {
		edgeValues = new int[4];
		edgeValues[0] = top;
		edgeValues[1] = left;
		edgeValues[2] = bottom;
		edgeValues[3] = right;
	}
	
	/**
	 * Returns top left bottom right edges
	 * @return
	 */
	public int[] getEdgeValues() {
		return edgeValues;
	}
	
	public void drawAt(PImage pg, int x, float y, Method method, boolean sqrt) {
		//System.out.println("HORIZ");
		for (int i=0;i<size();i++) {
			int xx = x + getPoint(i).x;
			float p1 = y-(int)y;
			float p0=1-p1;
			int y0=(int)y + getPoint(i).y;
			int y1=(int)y + 1 + getPoint(i).y;
			int c0 = pg.get(xx,y0);
			int c1 = pg.get(xx,y1);
			if (method.isPremultiplied()) {
				int[] outCol = renderRGB(c0, c1, p0, p1, getOpacity(i), getRGB(i), sqrt);
				pg.set(xx,y0,method.getMask()|outCol[0]);
				pg.set(xx,y1,method.getMask()|outCol[1]);
			} else if (method.equals(Method.JUST_ALPHA)) {
				int bgAlpha0 = c0>>24&0xff;
				int bgAlpha1 = c1>>24&0xff;
				int newAlpha0 = (int)( getOpacity(i) * p0 * 255 );
				int newAlpha1 = (int)( getOpacity(i) * p1 * 255 );
				if (newAlpha0>bgAlpha0) {
					pg.set(xx, y0, ((newAlpha0*bgAlpha0)>>8&0xff)<<24 | (c0&0x00ffffff) );
				}
				if (newAlpha1>bgAlpha1) {
					pg.set(xx, y1, ((newAlpha1*bgAlpha1)>>8&0xff)<<24 | (c1&0x00ffffff) );
				}
			}
		}
	}
	
	public void drawAt(PImage pg, float x, int y, Method method, boolean sqrt) {
		for (int i=0;i<size();i++) {
			float p1=x-(int)x;
			float p0=1-p1;
			int x0=(int)x + getPoint(i).x;
			int x1=(int)x + 1 + getPoint(i).x;
			int yy = y + getPoint(i).y;
			int c0 = pg.get(x0,yy);
			int c1 = pg.get(x1,yy);
			if (method.isPremultiplied()) {
				int[] outCol = renderRGB(c0, c1, p0, p1, getOpacity(i), getRGB(i), sqrt);
				pg.set(x0,yy,method.getMask()|outCol[0]);
				pg.set(x1,yy,method.getMask()|outCol[1]);
			} else if (method.equals(Method.JUST_ALPHA)) {
				int bgAlpha0 = c0>>24&0xff;
				int bgAlpha1 = c1>>24&0xff;
				int newAlpha0 = (int)( getOpacity(i) * p0 * 255 );
				int newAlpha1 = (int)( getOpacity(i) * p1 * 255 );
				if (newAlpha0>bgAlpha0) {
					pg.set(x0, yy, (newAlpha0&0xff)<<24 | (c0&0x00ffffff) );
				}
				if (newAlpha1>bgAlpha1) {
					pg.set(x1, yy, (newAlpha1&0xff)<<24 | (c1&0x00ffffff) );
				}
			}
		}
	}
	
	public void drawAt(PImage pg, int x, int y, Method method, boolean sqrt) {
		for (int i=0;i<size();i++) {
			int xx=x + getPoint(i).x;
			int yy=y + getPoint(i).y;
			int bg = pg.get(xx,yy);
			if (method.isPremultiplied()) {
				pg.set(xx,yy,0xff<<24|mixRGB(bg, getRGB(i),	(1-getOpacity(i)), getOpacity(i)));
			} else if (method.equals(Method.JUST_ALPHA)) {
				//TODO
			}
		}
	}
	
	
	
	private static int[] renderRGB(int bg0, int bg1, float p0, float p1, float opacity, int rgb, boolean sqrt) {
		if (sqrt) {
			p0 = (float)Math.sqrt(p0);
			p1 = (float)Math.sqrt(p1);
		}
		float dp0 = p0*opacity;
		float dp1 = p1*opacity;
		int col0 = 0, col1 = 0;
		col0 = mixRGB(bg0,rgb,(1-dp0),dp0);
		col1 = mixRGB(bg1,rgb,(1-dp1),dp1);
		return new int[] {col0,col1};
	}
	
	

}
