package pendrawing;

import java.awt.Point;

import pendrawing.brush.Brush;
import pendrawing.brush.BrushCreator;
import pendrawing.brush.BrushLine;
import pendrawing.brush.AbstractBrush.Method;
import processing.core.PApplet;

public class PenDrawing extends PApplet {

	private static final long serialVersionUID = 5864052341902352601L;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"pendrawing.PenDrawing"});
	}
	
	private final int thicknessSteps = 20;
	private final int thicknessMin = 5;  // diameter
	private final int thicknessMax = 20; // diameter
	private final float hardness = 0.2f;
	
	private final float innerOpacity = 1f;
	private final float outerOpacity = 0.0f;
	
	private Brush[] shapes;
	
	private int color = 0x3b2706;
	
	public void settings() {
		size(800,600,P3D);
	}
	
	public void setup() {
		
		background(240);
		
		shapes = new Brush[thicknessSteps];
		for (int i=0;i<shapes.length;i++) {
			float p = ((float)i/(shapes.length-1));
			float outerRadius = thicknessMin + p*thicknessMax;
			float innerRadius = outerRadius * hardness;
			shapes[i] = BrushCreator.createCircular(innerRadius, outerRadius, innerOpacity, outerOpacity, color);
		}
	}
	
	
	public void mousePressed() {
		//int i = (int)((shapes.length-1) *  (0.5-Math.cos(frameCount*0.05f)*0.5));
		int i = shapes.length-1;
		if (mouseButton==LEFT) {
			shapes[i].drawAt(this.g, mouseX, mouseY, Method.PREMULTIPLIED_RGB_TO_0XFF, true);
		} else if (mouseButton==RIGHT) {
			background(240);
			BrushLine l = new BrushLine((int)random(width),(int)random(height),(int)random(width),(int)random(height));
			l.drawVersion2(this.g, shapes[i]);
			//l.draw(this.g,shapes[i], false);
		}
	}
	
	public void mouseDragged() {
		if (mouseButton==LEFT) {
			//int i = (int)((shapes.length-1) *  (0.5-Math.cos(frameCount*0.05f)*0.5));
			int i = shapes.length-1;
			BrushLine l = new BrushLine(new Point(pmouseX,pmouseY),new Point(mouseX,mouseY));
			
			l.drawVersion2(this.g, shapes[i]);
			//l.draw(this.g,shapes[i],false);
		}
	}
	
	public void mouseReleased() {
	}
	
	
	public void draw() {
		//image(target,0,0);
	}

}
