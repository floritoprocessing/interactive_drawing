class Ant {
  double HARDNESS=0.8;     //  [0..1.0]
  double CURVEDNESS=0.5;  //  [0..0.5]
  double SPEED=1.0;
  double AGE=0;  // [0..100]
  double AGESPEED=0.01;
  
  Vec pos=new Vec();
  Vec mov=new Vec(SPEED,0,0);
  color col;
  int minX=0, maxX=10, minY=0, maxY=10;
  
  Ant(float x, float y, int mxX, int mxY, color[][] imgCol) {
    pos=new Vec((int)x,(int)y,0);
    maxX=mxX;
    maxY=mxY;
    mov.rotZ(2*Math.PI*Math.random());
    col=imgCol[(int)random(maxX)][(int)random(maxY)];
  }
  
  void setHardness(double h) { HARDNESS=h; }
  void setCurvedness(double c) { CURVEDNESS=c; }
  void setSpeed(double s) { SPEED=s; }
  void setAge(double a) { AGE=a; }
  void setAgeSpeed(double as) { AGESPEED=as; }
  void setColor(color c) { col=c; }
  
  void update(color[][] img, Vector allAnts, int id) {
    if (AGE<100.0) {
      // GET NEW DIRECTION:
      int r=1;
      Vec colMov=new Vec();
      float leastDiff=1.0;
      for (int xo=-r;xo<=r;xo++) {
        for (int yo=-r;yo<=r;yo++) {
          int nx=(int)pos.x+xo;
          int ny=(int)pos.y+yo;
          boolean overEdge=(nx<minX||nx>=maxX||ny<minY||ny>=maxY);
          if (!overEdge) {
            float cd=colorDifference(col,img[nx][ny]);
            if (cd<leastDiff) {
              leastDiff=cd;
              colMov.setVec(xo,yo,0);
            }
          }
        }
      }
      mov.add(vecMul(colMov,CURVEDNESS));
      mov.setLen(SPEED);
      // SET NEW POSITION AND DRAW:
      pos.add(mov);
      pos.constrX(minX,maxX);
      pos.constrY(minY,maxY);
      softSet((int)pos.x,(int)pos.y,col,(float)((1.0-AGE/100.0)*HARDNESS*pow((1.0-leastDiff),8)));
      //set((int)pos.x,(int)pos.y,col);
      AGE+=AGESPEED;
    } else {
      allAnts.remove(id);
    }
  }
  
  float colorDifference(color a, color b) {
    colorMode(RGB,255);
    return (abs(red(a)-red(b))+abs(green(a)-green(b))+abs(blue(a)-blue(b)))/765.0;
  }
}

void softSet(int x, int y, color c1, float p1) {
  float p2=1.0-p1;
  color c2=get(x,y);
  float nr=p1*red(c1)+p2*(red(c2));
  float ng=p1*green(c1)+p2*(green(c2));
  float nb=p1*blue(c1)+p2*(blue(c2));
  set(x,y,color(nr,ng,nb));
}
