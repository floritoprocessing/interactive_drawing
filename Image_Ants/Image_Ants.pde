/***************************
 * Image Ants
 * 
 * Ants that walk over an invisible image leaving traces
 *
 * properties:
 * 
 * setHardness(0..1);  
 * sets the drawing strength
 * 
 * setCurvedness(0.15);
 * 
 * setSpeed(0.7);
 * 
 * setAge(0);
 * 
 * setAgeSpeed(0.3);
 * 
 * created in 2005 by Marcus Graf
 * 
 ***************************/
 
 import java.util.Vector;

// nr of                 ants, hardness, curvedness, speed, age, agespeed, colorMode
double[][] PresetVal={  {  20,      0.2,        0.15,  0.7,   0,      0.3,   2 },
                        {  60,      0.5,        0.10,  1.4,   0,      1.0,   1 },
                        {  40,      0.8,        0.60,  2.5,   0,      0.7,   0 },
                        {  80,      0.4,        0.80,  8.5,   0,      1.7,   0 }   };
int presetNr=3;

Vector ants=new Vector();
int nrOfAntsPerDraw=(int)PresetVal[presetNr][0];
PImage drawImage;
color[][] img;
int lastFrame=0;

void setup() {
  drawImage=loadImage("TajMahalHorizontal.jpg");
  size(1000,511);//drawImage.width,drawImage.height);
//  size(1000,511);// taj mahal
  colorMode(RGB,255);
  background(0,0,0);

  img=new color[drawImage.width][drawImage.height];
  for (int x=0;x<width;x++) {
    for (int y=0;y<height;y++) {
      img[x][y]=drawImage.get(x,y);
    }
  }
}

void keyPressed() {
  int i = key-49;
  if (i>=0&&i<PresetVal.length) {
    presetNr = i;
  }
  //println(i);
}

void draw() {
  if (mousePressed) {
    for (int i=0;i<nrOfAntsPerDraw;i++) {
      float p=i/(float)nrOfAntsPerDraw;
      float xr=pmouseX-mouseX;
      float yr=pmouseY-mouseY;
      Ant tempAnt=new Ant(mouseX+p*xr,mouseY+p*yr,width,height,img);
      tempAnt.setHardness(PresetVal[presetNr][1]);
      tempAnt.setCurvedness(PresetVal[presetNr][2]);
      tempAnt.setSpeed(PresetVal[presetNr][3]);
      tempAnt.setAge(PresetVal[presetNr][4]);
      tempAnt.setAgeSpeed(PresetVal[presetNr][5]);
      
      switch ((int)PresetVal[presetNr][6]) {
        case 0: break;
        case 1: tempAnt.setColor(color(random(255),random(255),random(255))); break;
        case 2: float br=random(255); tempAnt.setColor(color(br,br,br)); break;
        default: break;
      }
      
      ants.add(tempAnt);
    }
  }

  do {
    for (int i=0;i<ants.size();i++) {
      Ant tempAnt=(Ant)ants.elementAt(i);
      tempAnt.update(img,ants,i);
    }
  } 
  while (millis()-lastFrame<40);
  lastFrame=millis();
}
