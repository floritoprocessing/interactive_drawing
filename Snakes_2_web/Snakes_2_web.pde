/*
moving your mouse across the screen creates snakes that craw and leave traces on the background
created by Marcus Graf in 2005
contact: marcus[AT]florito.net
*/

void setup() {
  // screensetup:
  size(400,300,P3D); colorMode(RGB,255); background(255,255,255);
  
  initImage();
  initSnakes();  // create array of 'dead' Snakes
  initBgImage(); // create an empty background buffer
}


void draw() {
  drawBgImage();  // draw background buffer
  growSnakes();   // draw snakes ( snakes will draw on background buffer )
}


void mouseMoved() {
  float mouseSpeed=0.2+0.8*constrain(dist(mouseX,mouseY,pmouseX,pmouseY),0,40)/40.0;
  float xnow=mouseX/(float)width;
  float ynow=mouseY/(float)width;
  for (int i=0;i<3;i++) {
    if (random(1)<mouseSpeed) {createNewSnake(xnow,ynow,mouseSpeed);}
  }
//  createNewSnake(mouseX/(float)width,mouseY/(float)width,mouseSpeed);
}


// RANDOM DRAWER:
float drawX,drawY;
void initRandomDraw() {
  
}


PImage img;
void initImage() {
  img=loadImage("Gogh800x600.jpg");
//  img=loadImage("Vermeer800x600.jpg");
  
  image(img, 0, 0, width, height);              // Displays the image from point (0,0) 
  
  // A new array to store the modified image 
  int[][] output = new int[width][height]; 
  int n2 = 3/2; 
  int m2 = 3/2; 
  float[][] kernel = { {-2, -2, -2}, 
                     {-2,  8, -2},                                        
                     {-2, -2, -2} }; 

  // Convolve the image 
  for(int y=0; y<height; y++) { 
    for(int x=0; x<width; x++) { 
      float sum = 0; 
      for(int k=-n2; k<n2; k++) { 
        for(int j=-m2; j<m2; j++) { 
          // Reflect x-j to not exceed array boundary 
          int xp = x-j; 
          int yp = y-k; 
          if (xp < 0) { 
            xp = xp + width; 
          } else if (x-j >= width) { 
            xp = xp - width; 
          } 
          // Reflect y-k to not exceed array boundary 
          if (yp < 0) { 
            yp = yp + height; 
          } else if (yp >= height) { 
            yp = yp - height; 
          } 
          sum = sum + kernel[j+m2][k+n2] * red(get(xp, yp)); 
        } 
      } 
      output[x][y] = int(sum);    
    } 
  }
  // Display the result of the convolution 
  // by copying new data into the pixel buffer 
  background(255,255,255);
  loadPixels(); 
  color c=color(0,0,0);
  for(int i=0; i<height; i++) { 
    for(int j=0; j<width; j++) { 
      c=color(output[j][i], output[j][i], output[j][i]);
      pixels[i*width + j] = mix(c,color(255,255,255),0.05) ; 
    } 
  } 
  updatePixels(); 
}
/*
 

 
 
 
 

 
 


 */





/* 
-------------------------------------------------------------------------------
------------------------------ background buffer ------------------------------
------------------------------------------------------------------------------- 
*/
  
color[] bg;

void initBgImage() { // copy screen into background buffer
  bg=new color[width*height]; 
  loadPixels();
  for (int i=0;i<pixels.length;i++) { bg[i]=pixels[i]; }
}

void drawBgImage() { // draw background buffer on screen
  for (int i=0;i<pixels.length;i++) { pixels[i]=bg[i]; }
  updatePixels();
}

/* draw pixel on background image
   x,y : screen position (float)
   c :   color to draw with
   p :   'pen pressure' to draw [0..1]
*/
void setBgImage2(float _x, float _y, int c, float p) { 
  for (float ix=_x-2;ix<=_x+2;ix++) {
    for (float iy=_y-2;iy<=_y+2;iy++) {
      float tp=dist(_x,_y,ix,iy);
      if (tp<1) {tp=1;} // <- Leave this line out for obscure effect
      int x=constrain(int(ix),0,width-1); 
      int y=constrain(int(iy),0,height-1);
      bg[y*width+x]=mix(c,bg[y*width+x],p/tp);
    }
  }
}

void setBgImage1(float _x, float _y, int c, float p) { 
  int x=constrain(int(_x),0,width-1); 
  int y=constrain(int(_y),0,height-1);
  bg[y*width+x]=mix(c,bg[y*width+x],p);
}

/* 
-----------------------------------------------------------------------------------
------------------------------ background buffer end ------------------------------
----------------------------------------------------------------------------------- 
*/ 
 

/* 
---------------------------------------------------------------------------------------
------------------------------ Snake class and functions ------------------------------
--------------------------------------------------------------------------------------- 
*/

int nrOfSnakes=1000;
Snake[] snake;

void initSnakes() {
  snake=new Snake[nrOfSnakes];
  for (int i=0;i<nrOfSnakes;i++) { snake[i]=new Snake(); }
}

void growSnakes() {
  for (int i=0;i<nrOfSnakes;i++) { if (!snake[i].dead) {snake[i].grow();} }
}

void createNewSnake(float _x, float _y, float _speed) {
  // find an available snake:
  int i=0; boolean found=false;
  while (i<nrOfSnakes&&!snake[i].dead) { i++; }
  if (i<nrOfSnakes) {
    // println("found one: "+i);
    // create a new snake:
    float direction=PI+atan2(pmouseY-mouseY,pmouseX-mouseX)+_speed*random(-HALF_PI,HALF_PI);
    snake[i].start(_x,_y,_speed,direction);
  }
}



class Snake {
  float maxX=1.0, maxY=maxX/(float)width*(float)height;
  float sScale=width;
  float[] x,y;     // positions;
  color[] drawColor;
  float direction=0;
  float speed=0;

  int len, maxLen=150; float lenPerc=0.0;
  float age, maxAge=200, agePerc=0.0;
  boolean dead=true;
  
  Snake() { len=0; age=0; x=new float[maxLen]; y=new float[maxLen]; drawColor=new color[maxLen]; }
  
  void start(float _x, float _y, float _speed, float _direction) {
    dead=false;
    len=0; x[0]=_x; y[0]=_y; speed=_speed;
    int tx=int(_x*img.width);
    int ty=int(_y/(float)width*(float)height*img.height);
    drawColor[0]=invert(img.get(tx,ty));
    age=0;
    direction=random(TWO_PI);
    direction=_direction;
  }
  
  void grow() {
    if (age<maxAge) { age++; } else {dead=true;}
    agePerc=age/maxAge;
  
    if (len<maxLen-1) {
      len++; lenPerc=len/(float)maxLen;
      direction+=random(-0.5,0.5);
      x[len]=x[len-1]+0.005*speed*(1-lenPerc)*cos(direction);
      y[len]=y[len-1]+0.005*speed*(1-lenPerc)*sin(direction);
      
      int tx=int(x[len]*img.width);
      int ty=int(y[len]/(float)width*(float)height*img.height);
      drawColor[len]=invert(img.get(tx,ty));
    }
    
    // draw:
    
    float press;
    for (int i=0;i<len;i++) {
      press=i/(float)len;
      sSet(x[i]*sScale,y[i]*sScale,drawColor[i],press*(1-agePerc));
    }
    if (len<maxLen-1) {
//      setBgImage2(x[len-1]*sScale,y[len-1]*sScale,invert(drawColor[len-1]),0.1);
      setBgImage1(x[len-1]*sScale,y[len-1]*sScale,invert(drawColor[len-1]),0.1);
    }
  }
  
}




// drawing functions:

void sSet(float _x, float _y, int c, float p) {
  int x=int(_x); int y=int(_y);
  int bg=get(x,y);
  set(x,y,mix(c,bg,p));
}

color mix(int a, int b, float p) {
  float q=1.0-p;
  int rr=int(ch_red(a)*p+ch_red(b)*q);
  int gg=int(ch_grn(a)*p+ch_grn(b)*q);
  int bb=int(ch_blu(a)*p+ch_blu(b)*q);
  return color(rr,gg,bb);
}

color invert(int a) {
  return color(255-ch_red(a),255-ch_grn(a),255-ch_blu(a));
}

int ch_red(int c) { return (c>>16&255); }
int ch_grn(int c) { return (c>>8&255); }
int ch_blu(int c) { return (c&255); }
