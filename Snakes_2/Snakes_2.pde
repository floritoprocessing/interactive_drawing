color white=color(255,255,255);

void setup() {
  size(800,600,P3D);
  colorMode(RGB,255);
  initSnakes();
  background(12,24,92);
  initBgImage();
}



void draw() {
//  background(12,24,92);
  drawBgImage();
  growSnakes();
}


void mouseMoved() {
  float mouseSpeed=0.2+0.8*constrain(dist(mouseX,mouseY,pmouseX,pmouseY),0,40)/40.0;
  if (random(1)<mouseSpeed) {createNewSnake(mouseX/(float)width,mouseY/(float)width,mouseSpeed);}
}


color[] bg;

void initBgImage() {
  bg=new color[width*height];
  loadPixels();
  for (int i=0;i<pixels.length;i++) {
    bg[i]=pixels[i];
  }
}
void drawBgImage() {
  for (int i=0;i<pixels.length;i++) {
    pixels[i]=bg[i];
  }
  updatePixels();
}
void setBgImage(float _x, float _y, int c, float p) {
  int x=constrain(int(_x),0,width-1); 
  int y=constrain(int(_y),0,height-1);
  bg[y*width+x]=mix(c,bg[y*width+x],p);
}


int nrOfSnakes=300;
Snake[] snake;

void initSnakes() {
  snake=new Snake[nrOfSnakes];
  for (int i=0;i<nrOfSnakes;i++) { snake[i]=new Snake(); }
}

void growSnakes() {
  for (int i=0;i<nrOfSnakes;i++) {
    if (!snake[i].dead) {snake[i].grow();}
  }
}


void createNewSnake(float _x, float _y, float _speed) {
  // find an available snake:
  int i=0; boolean found=false;
  while (i<nrOfSnakes&&!snake[i].dead) { i++; }
  if (i<nrOfSnakes) {
    println("found one: "+i);
    // create a new snake:
    float direction=PI+atan2(pmouseY-mouseY,pmouseX-mouseX)+_speed*random(-HALF_PI,HALF_PI);
    snake[i].start(_x,_y,_speed,direction);
  }
}



class Snake {
  float maxX=1.0, maxY=maxX/(float)width*(float)height;
  float sScale=width;
  float[] x,y;     // positions;
  float direction=0;
  float speed=0;
  color drawColor;
  boolean[] edge;  // over edge?
  int len, maxLen=200; float lenPerc=0.0;
  int age, maxAge=400; float agePerc=0.0;
  boolean dead=true;
  
  Snake() { len=0; age=0; x=new float[maxLen]; y=new float[maxLen]; edge=new boolean[maxLen]; }
  
  void start(float _x, float _y, float _speed, float _direction) {
    dead=false;
    len=0; x[0]=_x; y[0]=_y; speed=_speed;
    float Rintense=0.7+_speed*0.3-random(0.1);
    float Gintense=0.8+_speed*0.2-random(0.15);
    float Bintense=0.75+_speed*0.25-random(0.1);
    drawColor=color(255*Rintense,255,255*Bintense);
    age=0;
    direction=random(TWO_PI);
    direction=_direction;
  }
  
  void grow() {
    if (age<maxAge) {
      age++; 
    } else {dead=true;}
    agePerc=age/float(maxAge);
  
    if (len<maxLen-1) {
      len++; lenPerc=len/(float)maxLen;
      direction+=random(-0.3,0.3);
      x[len]=x[len-1]+0.005*speed*(1-agePerc)*cos(direction);
      y[len]=y[len-1]+0.005*speed*(1-agePerc)*sin(direction);
    }
    
    // draw:
    float press;
    for (int i=0;i<len;i++) {
      press=i/(float)len;
      sSet(x[i]*sScale,y[i]*sScale,drawColor,press*(1-agePerc));
    }
    if (len<maxLen-1) {
      setBgImage(x[len-1]*sScale,y[len-1]*sScale,drawColor,0.03);
    }
  }
  
}





void sSet(float _x, float _y, int c, float p) {
  int x=int(_x); int y=int(_y);
  int bg=get(x,y);
  set(x,y,mix(c,bg,p));
}

////////// screening modes:

int ch_red(int c) { return (c>>16&255); }
int ch_grn(int c) { return (c>>8&255); }
int ch_blu(int c) { return (c&255); }

int fade(int a, float p) {
  int rr=int(ch_red(a)*p);
  int gg=int(ch_grn(a)*p);
  int bb=int(ch_blu(a)*p);
  return (rr<<16|gg<<8|bb);
}

color mix(int a, int b, float p) {
  float q=1.0-p;
  int rr=int(ch_red(a)*p+ch_red(b)*q);
  int gg=int(ch_grn(a)*p+ch_grn(b)*q);
  int bb=int(ch_blu(a)*p+ch_blu(b)*q);
  return color(rr,gg,bb);
}


int multiply(int a, int b) {
  int rr=(ch_red(a)*ch_red(b))>>8;
  int gg=(ch_grn(a)*ch_grn(b))>>8;
  int bb=(ch_blu(a)*ch_blu(b))>>8;
  return (rr<<16|gg<<8|bb);
}

int screeen(int a, int b) {
  int rr=255 - ((255-ch_red(a)) * (255-ch_red(b))>>8);
  int gg=255 - ((255-ch_grn(a)) * (255-ch_grn(b))>>8);
  int bb=255 - ((255-ch_blu(a)) * (255-ch_blu(b))>>8);
  return (rr<<16|gg<<8|bb);
}

int lighten(int a, int b) {
  int rr=ch_red(a)>ch_red(b)?ch_red(a):ch_red(b);
  int gg=ch_grn(a)>ch_grn(b)?ch_grn(a):ch_grn(b);
  int bb=ch_blu(a)>ch_blu(b)?ch_blu(a):ch_blu(b);
  return (rr<<16|gg<<8|bb);
}
